<?php

declare(strict_types=1);

namespace Speedfin\Calculators;

use Speedfin\Calculators\DependencyInjection\FinancialCalculatorsExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FinancialCalculatorsBundle extends Bundle
{
    public function getContainerExtension(): ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = new FinancialCalculatorsExtension();
        }

        return $this->extension;
    }
}
