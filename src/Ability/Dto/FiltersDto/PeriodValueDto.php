<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Ability\Dto\FiltersDto;

final readonly class PeriodValueDto
{
    public function __construct(
        public ?int $value,
        public ?string $label
    ) {
    }
}
