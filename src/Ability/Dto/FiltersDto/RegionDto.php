<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Ability\Dto\FiltersDto;

final readonly class RegionDto
{
    public function __construct(
        public ?string $identifier,
        public ?string $label
    ) {
    }
}
