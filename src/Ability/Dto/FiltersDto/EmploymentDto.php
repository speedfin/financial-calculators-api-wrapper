<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Ability\Dto\FiltersDto;

final readonly class EmploymentDto
{
    public function __construct(
        public ?int $id,
        public ?string $name,
        public ?int $periodForward,
        public ?int $periodFrom,
        public  ?int $value,
        /**
         * @var PeriodValueDto[]|null
         */
        public ?array $values
    ) {
    }
}
