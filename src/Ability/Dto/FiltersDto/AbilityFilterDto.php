<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Ability\Dto\FiltersDto;

final readonly class AbilityFilterDto
{
    public function __construct(
        /**
         * @var EmploymentDto[]|null
         */
        public ?array $employments,
        /**
         * @var CommitmentDto[]|null
         */
        public ?array $commitments,
        /**
         * @var RegionDto[]|null
         */
        public ?array $regions,
        /**
         * @var RealEstateDto[]|null
         */
        public ?array $realEstates
    ){
    }

    public function getEmploymentById(int $id): ?EmploymentDto
    {
        /** @var EmploymentDto $employment */
        foreach ($this->employments as $employment) {
            if ($employment->id === $id) {
                return $employment;
            }
        }

        return null;
    }

    public function getCommitmentById(int $id): ?CommitmentDto
    {
        /** @var CommitmentDto $commitment */
        foreach ($this->commitments as $commitment) {
            if ($commitment->id === $id) {
                return $commitment;
            }
        }

        return null;
    }
}
