<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Ability\Dto\FiltersDto;

final readonly class CommitmentDto
{
    public function __construct(
        public ?int $id,
        public ?string $name,
        public ?int $period,
        public ?int $limit,
        public ?string $currency,
        public ?int $value
    ){
    }
}
