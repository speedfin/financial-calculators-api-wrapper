<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Currency\Service;

use Speedfin\Calculators\Common\Service\AbstractProduct;
use Speedfin\Calculators\Currency\Adapter\CurrencyAdapter;
use Speedfin\Calculators\Currency\Dto\CurrencyFilterDto;
use Speedfin\Calculators\Currency\Dto\CurrencyListDto;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class Currency extends AbstractProduct implements CurrencyInterface
{
    public function getList(?CurrencyFilterDto $filter = null): CurrencyListDto
    {
        $currencyAdapter = new CurrencyAdapter($this->getUrl(self::LIST_API_ENDPOINT), $this->token);
        $cache = new FilesystemAdapter(
            '',
            14400,
            $this->cacheDir
        );

        return $cache->get(
            sprintf('currency-list-%s', md5(json_encode((array) $filter))),
            function (ItemInterface $item) use ($currencyAdapter, $filter) {
                return $currencyAdapter->getCollection($filter);
        });
    }
}