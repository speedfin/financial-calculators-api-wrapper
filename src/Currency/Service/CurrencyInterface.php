<?php

namespace Speedfin\Calculators\Currency\Service;

use Speedfin\Calculators\Common\Service\JWTInterface;
use Speedfin\Calculators\Currency\Dto\CurrencyFilterDto;
use Speedfin\Calculators\Currency\Dto\CurrencyListDto;

interface CurrencyInterface extends JWTInterface
{
    const LIST_API_ENDPOINT = 'api/v3/currency';

    public function getList(?CurrencyFilterDto $filter = null): CurrencyListDto;
}