<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Currency\Adapter;

use Speedfin\Calculators\Common\Adapter\AbstractAdapter;
use Speedfin\Calculators\Currency\Dto\CurrencyFilterDto;
use Speedfin\Calculators\Currency\Dto\CurrencyListDto;
use Speedfin\Calculators\Service\Deserialize;

class CurrencyAdapter extends AbstractAdapter
{
    public function getCollection(CurrencyFilterDto $filters): CurrencyListDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), CurrencyListDto::class);
    }
}