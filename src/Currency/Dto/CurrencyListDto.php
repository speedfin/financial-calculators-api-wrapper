<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Currency\Dto;

final readonly class CurrencyListDto
{
    public function __construct(
        /**
         * @var CurrencyDto[]|null
         */
        public ?array $items
    ) {
    }
}
