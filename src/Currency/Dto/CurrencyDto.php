<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Currency\Dto;

final readonly class CurrencyDto
{
    public function __construct(
        public ?int $id,
        public ?string $name,
        public ?bool $defaultSelected,
        public ?float $currentValue
    ) {
    }
}
