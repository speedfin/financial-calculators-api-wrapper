<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Currency\Dto;

final readonly class CurrencyFilterDto
{
    public function __construct(
        public ?string $productType,
        public ?int $firmId
    ) {
    }
}
