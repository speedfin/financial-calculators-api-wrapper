<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class InstallmentDto
{
    public function __construct(
        public ?DecreasingDto $decreasing,
        public ?EqualDto $equal,
        public ?string $periodChangesJson
    ) {
    }
}
