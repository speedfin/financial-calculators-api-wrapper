<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class MarginDto
{
    public function __construct(
        public ?float $value,
        public ?float $valueManual,
        public ?float $futureValue = 2.00
    ) {
    }
}
