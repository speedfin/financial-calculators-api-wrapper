<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class ExpertValueDto
{
    public function __construct(
        public ?int $otherCost,
        public ?string $comment
    ) {
    }
}
