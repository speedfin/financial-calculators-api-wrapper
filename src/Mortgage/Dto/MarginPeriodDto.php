<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class MarginPeriodDto
{
    public function __construct(
        public ?int $periodFrom,
        public ?int $periodTo,
        public ?float $interestRate,
        public ?float $surchargeRate,
        public ?float $surchargeCreditValue,
        public ?float $surchargeCorrectionValue
    ) {
    }
}
