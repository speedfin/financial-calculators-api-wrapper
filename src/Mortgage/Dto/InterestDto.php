<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

use Speedfin\Calculators\Common\Enum\InterestType;

final readonly class InterestDto
{
    public function __construct(
        public mixed $value,
        public ?string $type,
        public ?FixedInterestRateDto $fixedInterestRate
    ) {
    }

    public function isFixedInterestRate(): bool
    {
        return $this->type === InterestType::FIXED_INTEREST_RATE_TYPE->value;
    }
}
