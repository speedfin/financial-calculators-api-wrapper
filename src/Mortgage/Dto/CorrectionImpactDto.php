<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class CorrectionImpactDto
{
    public function __construct(
        public ?bool $creditingPossibilityExceeded,
        public ?bool $forceCreditingPossibility,
        public ?float $manualMarginFromCheckNotRequired,
        public ?float $manualFixedInterestRateFromCheckNotRequired,
        public ?float $manualProvisionFromCheckNotRequired,
    ) {
    }
}
