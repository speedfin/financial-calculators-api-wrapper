<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class DecreasingDto
{
    public function __construct(
        public mixed $monthly,
        public mixed $monthlyTotal,
        public mixed $total,
        public ?FixedInterestRateInstallmentDto $fixedInterestRate,
        public ?PeriodChangeDto $periodChange,
        public ?float $interestValue,
        public ?bool $enabled
    ) {
    }
}
