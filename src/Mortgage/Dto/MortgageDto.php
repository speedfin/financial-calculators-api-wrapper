<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class MortgageDto
{
    public function __construct(
        public ?ProsConsDto $prosCons,
        public ?BankDto $bank,
        public ?ExpertValueDto $expertValue,
        public ?int $id,
        public ?int $cloneId,
        public ?string $requestId,
        public ?string $title,
        public ?string $description,
        public ?CurrencyIndexDto $currencyIndex,
        public ?SurchargeDto $surcharge,
        public ?CurrencyDto $currency,
        public ?InsuranceDto $insurance,
        public ?InstallmentDto $installment,
        public ?AbilityDto $ability,
        public ?CostDto $cost,
        public ?MarginDto $margin,
        public ?array $margins,
        public ?float $surchargeIndexRate,
        public ?CreditPeriodDto $creditPeriod,
        public ?LtvDto $ltv,
        public ?ProvisionDto $provision,
        public ?InterestDto $interest,
        public ?RequirementDto $requirement,
        public ?PromotionDto $promotion,
        public ?CorrectionImpactDto $correctionImpact,
        public ?RrsoDto $rrso
    ) {
    }
}
