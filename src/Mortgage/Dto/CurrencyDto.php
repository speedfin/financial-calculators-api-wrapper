<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class CurrencyDto
{
    public function __construct(
        public ?int $id,
        public ?float $value,
        public ?string $iso,
        public ?string $name,
        public ?string $symbol,
        public ?bool $foreignCurrency,
        public ?bool $showForeign
    ) {
    }

    public function isForeignCurrency(): bool
    {
        return $this->foreignCurrency === true;
    }
}
