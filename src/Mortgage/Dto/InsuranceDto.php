<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class InsuranceDto
{
    public function __construct(
        /**
         * @var CorrectionDto[]
         */
        public ?array $lowContribution,
        /**
         * @var CorrectionDto[]
         */
        public ?array $bridgeInsurance,
        /**
         * @var CorrectionDto[]
         */
        public ?array $lifeInsurance,
        /**
         * @var CorrectionDto[]
         */
        public ?array $employInsurance,
        /**
         * @var CorrectionDto[]
         */
        public ?array $propertyInsurance,
        /**
         * @var CorrectionDto[]
         */
        public ?array $otherInsurance,
        /**
         * @var CorrectionDto[]
         */
        public ?array $earlyRepayment,
        /**
         * @var CorrectionDto[]
         */
        public ?array $accountManagement,
        /**
         * @var CorrectionDto[]
         */
        public ?array $creditCard,
        /**
         * @var CorrectionDto[]
         */
        public ?array $valuation
    ) {
    }
}
