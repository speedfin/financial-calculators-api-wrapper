<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class SurchargeDto
{
    public function __construct(
        public ?float $rate,
        public ?float $value
    ) {
    }
}
