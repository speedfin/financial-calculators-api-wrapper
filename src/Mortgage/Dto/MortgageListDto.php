<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class MortgageListDto
{
    public function __construct(
        /**
         * @var MortgageDto[]
         */
        public ?array $items
    ) {
    }
}
