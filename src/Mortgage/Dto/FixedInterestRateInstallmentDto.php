<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class FixedInterestRateInstallmentDto
{
    public function __construct(
        public ?float $installmentValue,
        public ?float $value
    ) {
    }
}
