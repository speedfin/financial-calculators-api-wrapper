<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\CollectionFilterDto;

final readonly class CollectionFilterDto
{
    public function __construct(
        /**
         * @var PropertySortDto[]|null
         */
        public ?array $propertySorts,
        /**
         * @var PropertySortDto[]|null
         */
        public ?array $loanPropertySorts,
        /**
         * @var CreditPurposeDto[]|null
         */
        public ?array $creditPurposes,
        /**
         * @var AdditionalDto[]|null
         */
        public ?array $additional
    ) {
    }

    public function getPropertySorts(?int $loan): array
    {
        return $loan ? $this->loanPropertySorts : $this->propertySorts;
    }
}
