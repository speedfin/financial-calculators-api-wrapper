<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\CollectionFilterDto;

final readonly class CreditPurposeDto
{
    public function __construct(
        public ?int $id,
        public ?string $name
    ) {
    }
}
