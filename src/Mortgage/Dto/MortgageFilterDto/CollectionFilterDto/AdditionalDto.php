<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\CollectionFilterDto;

final readonly class AdditionalDto
{
    public function __construct(
        public ?string $identifier,
        public ?string $label
    ) {
    }
}
