<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\CollectionFilterDto;

final readonly class PropertySortDto
{
    public function __construct(
        public ?string $name,
        public ?int $id,
        public ?int $primaryMarket
    ) {
    }
}
