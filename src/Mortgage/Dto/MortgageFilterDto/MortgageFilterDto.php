<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto;

use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\AbilityFilterDto\AbilityFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto\ItemFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\SliceAndSortableFilterDto\SliceFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\SliceAndSortableFilterDto\SortableFilterDto;

final readonly class MortgageFilterDto
{
    public function __construct(
        public ?bool $loan = false,
        public ?array $properties = [],
        public ?array $currencies = [],
        public ?AbilityFilterDto $ability = null,
        public ?bool $fixedInterestRate = null,
        public ?bool $variableInterestRate = null,
        public ?bool $periodicalInterestRate = null,
        public ?bool $indexWibor = true,
        public ?bool $indexWiron = true,
        public ?SliceFilterDto $slice = null,
        public ?SortableFilterDto $sortable = null,
        public ?array $banks = null,
        public ?array $ids = null,
        public int $firmId = 1,
        public ?bool $clientInternal = true,
        public ?bool $clientExternal = true,
        public ?int $creditValue = 250000,
        public ?int $secureValue = 350000,
        public ?int $creditPeriod = 360,
        public ?int $creditPurpose = null,
        public ?int $propertySort = null,
        public ?float $currencyIndex= null,
        public ?ItemFilterDto $item = null,
        public ?bool $eco = false,
        public ?bool $rkm = false,
        public ?bool $bestBankOffer = false,
        public ?int $creditClientAge = 30
    ) {
    }

    public function getLtv(): float
    {
        return round(($this->creditValue / $this->secureValue * 100), 2);
    }
}
