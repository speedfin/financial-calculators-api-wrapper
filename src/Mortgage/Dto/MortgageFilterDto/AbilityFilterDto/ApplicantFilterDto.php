<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\AbilityFilterDto;

final readonly class ApplicantFilterDto
{
    const APPLICANT = 'Wnioskodawca';

    public function __construct(
        public ?string $birthday = null,
        public string $name = self::APPLICANT,
        /**
         * @var OutcomeFilterDto[]
         */
        public ?array $outcomes = [],
        /**
         * @var IncomeFilterDto[]
         */
        public ?array $incomes = []
    ) {
    }
}
