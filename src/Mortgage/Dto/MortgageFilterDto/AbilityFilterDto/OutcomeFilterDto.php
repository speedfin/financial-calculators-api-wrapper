<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\AbilityFilterDto;

final readonly class OutcomeFilterDto
{

    public function __construct(
        public ?int $id = null,
        public ?int $value = null,
        public ?int $limit = null,
        public ?int $currency = null,
        public ?int $period = null
    ) {
    }
}
