<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\AbilityFilterDto;

final readonly class HouseholdFilterDto
{
    const FLAT = 'Mieszkanie';
    const OTHER = 'Pozostałe';

    public function __construct(
        public ?int $cars = null,
        public ?int $people = null,
        public ?int $area = null,
        public string $status = self::FLAT,
        public string $region = self::OTHER,
        /**
         * @var ApplicantFilterDto[]
         */
        public ?array $applicants = []
    ) {
    }
}
