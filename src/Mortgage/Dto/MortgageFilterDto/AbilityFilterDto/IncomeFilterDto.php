<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\AbilityFilterDto;

final readonly class IncomeFilterDto
{

    public function __construct(
        public ?int $id = null,
        public ?int $value = null,
        public ?int $periodFrom = null,
        public ?int $periodForward = null
    ) {
    }
}
