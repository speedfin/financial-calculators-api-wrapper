<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\AbilityFilterDto;

final readonly class AbilityFilterDto
{
    public function __construct(
        /**
         * @var HouseholdFilterDto[]
         */
        public ?array $households = []
    ) {
    }
}
