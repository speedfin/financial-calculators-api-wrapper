<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class CurrencyFilterDto
{
    public function __construct(
        public ?int $id = null,
        public ?float $value = null,
        public ?string $iso = null,
        public ?string $symbol = null,
        public ?bool $showForeign = null
    ) {
    }
}
