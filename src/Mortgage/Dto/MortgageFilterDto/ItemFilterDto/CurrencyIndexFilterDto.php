<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class CurrencyIndexFilterDto
{
    public function __construct(
        public ?int $id = null,
        public ?float $value = null
    ) {
    }
}
