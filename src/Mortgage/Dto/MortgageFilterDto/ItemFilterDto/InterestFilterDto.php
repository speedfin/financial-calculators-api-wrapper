<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class InterestFilterDto
{
    public function __construct(
        public ?float $value = null,
        public ?FixedInterestRateFilterDto $fixedInterestRate = null
    ) {
    }
}
