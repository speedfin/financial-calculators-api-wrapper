<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class CostFilterDto
{
    public function __construct(
        public ?float $total = null,
        public ?float $totalWithPropertyInsurance = null,
    ) {
    }
}
