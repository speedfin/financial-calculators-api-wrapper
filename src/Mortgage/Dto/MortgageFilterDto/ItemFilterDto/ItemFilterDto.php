<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class ItemFilterDto
{
    public function __construct(
        public ?CurrencyIndexFilterDto $currencyIndex = null,
        public ?InterestFilterDto $interest = null,
        public ?AbilityFilterDto $ability = null,
        public ?LtvFilterDto $ltv  = null,
        public ?CurrencyFilterDto $currency = null,
        public ?BankFilterDto $bank = null,
        public ?MarginFilterDto $margin = null,
        public ?ProvisionFilterDto $provision = null,
        public ?ExpertValueFilterDto $expertValue = null,
        public ?CurrentFilterDto $current = null,
        /**
         * @var CorrectionFilterDto[]
         */
        public ?array $corrections = [],
        public ?int $id = null,
        public ?int $creditValueGross = null,
        public ?string $requestId = null
    ) {
    }
}
