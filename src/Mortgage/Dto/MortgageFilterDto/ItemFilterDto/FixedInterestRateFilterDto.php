<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class FixedInterestRateFilterDto
{
    public function __construct(
        public ?float $valueManual = null,
        public ?float $value = null,
        public ?int $periodFrom = null,
        public ?int $periodTo = null
    ) {
    }
}
