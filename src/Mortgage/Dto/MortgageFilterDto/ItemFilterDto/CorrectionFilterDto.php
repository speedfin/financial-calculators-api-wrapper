<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class CorrectionFilterDto
{
    public function __construct(
        public ?int $id = null,
        public ?bool $checked = null,
        public ?bool $credited = null
    ) {
    }
}
