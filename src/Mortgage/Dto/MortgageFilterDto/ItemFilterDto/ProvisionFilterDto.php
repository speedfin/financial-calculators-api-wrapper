<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class ProvisionFilterDto
{
    public function __construct(
        public ?float $percent = null,
        public ?float $percentManual = null,
        public ?bool $credited = null
    ) {
    }
}
