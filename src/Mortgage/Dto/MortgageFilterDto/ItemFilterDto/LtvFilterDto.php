<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class LtvFilterDto
{
    public function __construct(
        public ?float $value = null,
        public ?float $maxValue = null
    ) {
    }
}
