<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class BankFilterDto
{
    public function __construct(
        public ?int $id = null
    ) {
    }
}
