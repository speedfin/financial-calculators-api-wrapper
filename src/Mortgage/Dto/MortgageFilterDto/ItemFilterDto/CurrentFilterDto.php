<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class CurrentFilterDto
{
    public function __construct(
        public ?string $name = null,
        public ?string $value = null
    ) {
    }
}
