<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class MarginFilterDto
{
    public function __construct(
        public ?float $value = null,
        public ?float $valueManual = null
    ) {
    }
}
