<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\ItemFilterDto;

final readonly class ExpertValueFilterDto
{
    public function __construct(
        public ?int $otherCost = null,
        public ?string $comment = null,
    ) {
    }
}
