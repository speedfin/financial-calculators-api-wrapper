<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto;

final readonly class MortgageArchiveFilterDto
{
      public function __construct(
          /**
           * @var MortgageFilterDto[]
           */
        public ?array $filters
      ) {
      }
}
