<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\SliceAndSortableFilterDto;

final readonly class SortableFilterDto
{
    const DIRECTION_ASC = 'ASC';
    public function __construct(
        public ?string $subField,
        public ?bool $bankShuffle,
        public ?bool $bankUnique,
        public string $field = 'id',
        public string $direction = self::DIRECTION_ASC
    ) {
    }
}
