<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class AbilityDto
{
    public function __construct(
        public ?float $value,
        public ?float $income,
        public ?string $info,
        public ?string $employmentDescription
    ) {
    }
}
