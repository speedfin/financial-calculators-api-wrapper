<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class ProsConsDto
{
    public function __construct(
        public ?array $pros,
        public ?array $cons,
        public ?string $prosCons
    ) {
    }
}
