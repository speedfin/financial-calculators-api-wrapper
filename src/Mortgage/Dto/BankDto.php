<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class BankDto
{
    public function __construct(
        public ?int $id,
        public ?string $slug,
        public ?string $name,
        public ?int $processingTime,
        public ?LogoDto $logo
    ) {
    }
}
