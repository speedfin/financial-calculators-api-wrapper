<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Dto;

final readonly class FixedInterestRateDto
{
    public function __construct(
        public int|string|null $periodFrom,
        public int|string|null $periodTo,
        public float|string|null $value,
        public ?float $valueManual
    ) {
    }
}
