<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Adapter;

use Speedfin\Calculators\Common\Adapter\AbstractAdapter;

class MortgageAbilityFilterAdapter extends AbstractAdapter
{
    public function getFilter(): array
    {
        return $this->sendGetRequest()->toArray();
    }
}