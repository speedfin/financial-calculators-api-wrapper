<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Adapter;

use Speedfin\Calculators\Common\Adapter\AbstractAdapter;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\MortgageArchiveFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\MortgageFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageListDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageDto;
use Speedfin\Calculators\Service\Deserialize;

class MortgageAdapter extends AbstractAdapter
{
    public function getCollection(MortgageFilterDto $filters): MortgageListDto
{
    $data = $this->sendGetRequest((array) $filters);

    return Deserialize::deserialize($data->getContent(), MortgageListDto::class);
}

    public function getArchiveCollection(MortgageArchiveFilterDto $filters): MortgageListDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), MortgageListDto::class);
    }

    public function getItem(MortgageFilterDto $filters): MortgageDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), MortgageDto::class);
    }
}
