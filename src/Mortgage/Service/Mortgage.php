<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Mortgage\Service;

use Psr\Cache\InvalidArgumentException;
use Speedfin\Calculators\Ability\Dto\FiltersDto\AbilityFilterDto;
use Speedfin\Calculators\Common\Service\AbstractProduct;
use Speedfin\Calculators\Mortgage\Adapter\MortgageAbilityFilterAdapter;
use Speedfin\Calculators\Mortgage\Adapter\MortgageAdapter;
use Speedfin\Calculators\Mortgage\Adapter\MortgageCollectionFilterAdapter;
use Speedfin\Calculators\Mortgage\Dto\MortgageDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\CollectionFilterDto\CollectionFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\MortgageArchiveFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\MortgageFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageListDto;
use Speedfin\Calculators\Service\Deserialize;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class Mortgage extends AbstractProduct implements MortgageInterface
{
    /**
     * @throws InvalidArgumentException
     */
    public function calculate(MortgageFilterDto $filter): MortgageListDto
    {
        $mortgageAdapter = new MortgageAdapter($this->getUrl(self::LIST_API_ENDPOINT), $this->token);
        return $mortgageAdapter->getCollection($filter);
        if ($this->cache === false || !$filter->firmId) {
            return $mortgageAdapter->getCollection($filter);
        }
        $filesystemAdapter = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );
        return $filesystemAdapter->get($this->getCacheKey($filter, sprintf('mortgage-list-%s', md5(json_encode((array) $filter)))),
            function (ItemInterface $item) use ($mortgageAdapter, $filter) {
                return $mortgageAdapter->getCollection($filter);
        });
    }

    /**
     * @throws InvalidArgumentException
     */
    public function calculateItem(int $id, MortgageFilterDto $filter): MortgageDto
    {
        $mortgageAdapter = new MortgageAdapter(
            str_replace('{id}', (string)$id, $this->getUrl(self::ITEM_API_ENDPOINT)),
            $this->token
        );
        if ($this->cache === false) {
            return $mortgageAdapter->getItem($filter);
        }
        $filesystemAdapter = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );
        return $filesystemAdapter->get($this->getCacheKey($filter, sprintf('mortgage-item-%s-%s', (string) $id, md5(json_decode((array) $filter)))),
            function (ItemInterface $item) use ($mortgageAdapter, $filter) {
                return $mortgageAdapter->getItem($filter);
        });
    }

    public function calculateArchive(MortgageArchiveFilterDto $filter): MortgageListDto
    {
        $mortgageAdapter = new MortgageAdapter(
            $this->getUrl(self::LIST_ARCHIVE_API_ENDPOINT),
            $this->token
        );

        return $mortgageAdapter->getArchiveCollection($filter);
    }

    public function calculateList(MortgageArchiveFilterDto $filter): MortgageListDto
    {
        $mortgageAdapter = new MortgageAdapter(
            $this->getUrl(self::LIST_PRINT_API_ENDPOINT),
            $this->token
        );

        return $mortgageAdapter->getArchiveCollection($filter);
    }

    public function getAbilityFilters(): AbilityFilterDto
    {
        $cache = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );

        $token = $this->token;
        return $cache->get('mortgage-ability-filters', function (ItemInterface $item) use ($token) {
            $regionAdapter = new MortgageAbilityFilterAdapter($this->getUrl(self::ABILIY_FILTER_REGION_API_ENDPOINT), $token);
            $realEstateAdapter = new MortgageAbilityFilterAdapter($this->getUrl(self::ABILIY_FILTER_REAL_ESTATE_API_ENDPOINT), $token);
            $employmentAdapter = new MortgageAbilityFilterAdapter($this->getUrl(self::ABILIY_FILTER_EMPLOYMENT_API_ENDPOINT), $token);
            $commitmentAdapter = new MortgageAbilityFilterAdapter($this->getUrl(self::ABILIY_FILTER_COMMITMENT_API_ENDPOINT), $token);

            $data = [
                'regions' => $regionAdapter->getFilter(),
                'realEstates' => $realEstateAdapter->getFilter(),
                'employments' => $employmentAdapter->getFilter(),
                'commitments' => $commitmentAdapter->getFilter(),
            ];

            return Deserialize::deserialize(json_encode($data), AbilityFilterDto::class);
        });
    }

    public function getCollectionFilters(): CollectionFilterDto
    {
        $cache = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );

        $token = $this->token;
        return $cache->get('mortgage-collection-filters', function (ItemInterface $item) use ($token) {
            $propertySortAdapter = new MortgageCollectionFilterAdapter($this->getUrl(self::COLLECTION_FILTER_PROPERTY_SORT_API_ENDPOINT), $token);
            $loanPropertySortAdapter = new MortgageCollectionFilterAdapter($this->getUrl(self::COLLECTION_FILTER_LOAN_PROPERTY_SORT_API_ENDPOINT), $token);
            $creditPurposeAdapter = new MortgageCollectionFilterAdapter($this->getUrl(self::COLLECTION_FILTER_CREDIT_PURPOSE_API_ENDPOINT), $token);
            $additionalAdapter = new MortgageCollectionFilterAdapter($this->getUrl(self::COLLECTION_FILTER_ADDITIONAL_API_ENDPOINT), $token);

            $data = [
                'propertySorts' => $propertySortAdapter->getFilter(),
                'loanPropertySorts' => $loanPropertySortAdapter->getFilter(),
                'creditPurposes' => $creditPurposeAdapter->getFilter(),
                'additional' => $additionalAdapter->getFilter(),
            ];

           return Deserialize::deserialize(json_encode($data), CollectionFilterDto::class);
        });
    }
}
