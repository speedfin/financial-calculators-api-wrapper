<?php

namespace Speedfin\Calculators\Mortgage\Service;

use Speedfin\Calculators\Ability\Dto\FiltersDto\AbilityFilterDto;
use Speedfin\Calculators\Common\Service\JWTInterface;
use Speedfin\Calculators\Mortgage\Dto\MortgageDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\CollectionFilterDto\CollectionFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\MortgageArchiveFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\MortgageFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageListDto;

interface MortgageInterface extends JWTInterface
{
    const LIST_API_ENDPOINT = 'api/v3/mortgage';
    const LIST_ARCHIVE_API_ENDPOINT = 'api/v3/mortgage/archive';
    const LIST_PRINT_API_ENDPOINT = 'api/v2/mortgage/list';
    const ITEM_API_ENDPOINT = 'api/v2/mortgage/{id}';

    const ABILIY_FILTER_REGION_API_ENDPOINT = 'api/v2/mortgage/filter/ability/region';
    const ABILIY_FILTER_REAL_ESTATE_API_ENDPOINT = 'api/v2/mortgage/filter/ability/real_estate';
    const ABILIY_FILTER_COMMITMENT_API_ENDPOINT = 'api/v2/mortgage/filter/ability/commitment';
    const ABILIY_FILTER_EMPLOYMENT_API_ENDPOINT = 'api/v2/mortgage/filter/ability/employment';
    const COLLECTION_FILTER_PROPERTY_SORT_API_ENDPOINT = 'api/v2/mortgage/filter/collection/property_sort/1';
    const COLLECTION_FILTER_LOAN_PROPERTY_SORT_API_ENDPOINT = 'api/v2/mortgage/filter/collection/property_sort/2';
    const COLLECTION_FILTER_CREDIT_PURPOSE_API_ENDPOINT = 'api/v2/mortgage/filter/collection/credit_purpose';
    const COLLECTION_FILTER_ADDITIONAL_API_ENDPOINT = 'api/v2/mortgage/filter/collection/additional';

    public function calculate(MortgageFilterDto $filter): MortgageListDto;

    public function calculateItem(int $id, MortgageFilterDto $filter): MortgageDto;

    public function calculateArchive(MortgageArchiveFilterDto $filter): MortgageListDto;
    
    public function calculateList(MortgageArchiveFilterDto $filter): MortgageListDto;
    
    public function getAbilityFilters(): AbilityFilterDto;

    public function getCollectionFilters(): CollectionFilterDto;

    public function setCache(bool $cache): void;
}