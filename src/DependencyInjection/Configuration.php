<?php

declare(strict_types=1);

namespace Speedfin\Calculators\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('speedfin_calculators');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('api_domain')
                    ->isRequired()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
