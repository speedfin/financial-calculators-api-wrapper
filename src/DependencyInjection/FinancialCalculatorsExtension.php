<?php

declare(strict_types=1);

namespace Speedfin\Calculators\DependencyInjection;

use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

final class FinancialCalculatorsExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(\dirname(__DIR__).'/Resources/config'));
        $loader->load('services.yaml');

        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $services = [
            'speedfin.calculators.mortgage',
            'speedfin.calculators.money_loan',
            'speedfin.calculators.company_loan',
            'speedfin.calculators.bank',
            'speedfin.calculators.currency',
            'speedfin.calculators.installment',
        ];

        foreach ($services as $serviceName) {
            $this->injectConfig($serviceName, $config, $container);
        }
    }

    private function injectConfig(string $serviceName, array $config, ContainerBuilder $container)
    {
        $helperDefinition = $container->getDefinition($serviceName);
        $helperDefinition->replaceArgument(0, $config['api_domain']);
    }
    
    public function getAlias(): string
    {
        return 'speedfin_financial_calculators';
    }
}
