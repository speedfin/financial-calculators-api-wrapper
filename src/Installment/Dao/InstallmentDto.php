<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Installment\Dao\InstallmentDto\DecreasingDto;
use Speedfin\Calculators\Installment\Dao\InstallmentDto\EqualDto;
use Speedfin\Calculators\Installment\Dao\InstallmentDto\ScheduleDto;

class InstallmentDto extends DataTransferObject
{
    public ?EqualDto $equal;
    public ?DecreasingDto $decreasing;
    #[CastWith(ArrayCaster::class, itemType: ScheduleDto::class)]
    public ?array $equalSchedule;
    #[CastWith(ArrayCaster::class, itemType: ScheduleDto::class)]
    public ?array $decreasingSchedule;
}