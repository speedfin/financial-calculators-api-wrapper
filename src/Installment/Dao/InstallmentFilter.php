<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;
use Speedfin\Calculators\Installment\Dao\InstallmentFilter\CurrencyFilter;
use Speedfin\Calculators\Installment\Dao\InstallmentFilter\FixedInterestRateFilter;
use Speedfin\Calculators\Installment\Dao\InstallmentFilter\PeriodChangeFilter;
use Speedfin\Calculators\Installment\Dao\InstallmentFilter\PeriodMarginFilter;

class InstallmentFilter extends DataTransferObject
{
    public ?int $creditValue;
    public ?int $creditPeriod;
    #[CastWith(FloatCaster::class)]
    public ?float $interest;
    #[CastWith(FloatCaster::class)]
    public ?float $currencyIndex;
    #[CastWith(FloatCaster::class)]
    public ?float $margin;
    #[CastWith(FloatCaster::class)]
    public ?float $surchargeRate;
    #[CastWith(FloatCaster::class)]
    public ?float $surchargeIndexRate;
    public ?FixedInterestRateFilter $fixedInterestRate;
    public ?CurrencyFilter $currency;
    #[CastWith(ArrayCaster::class, itemType: PeriodMarginFilter::class)]
    public ?array $margins = [];
    public ?string $interestType;
    #[CastWith(ArrayCaster::class, itemType: PeriodChangeFilter::class)]
    public ?array $marginPeriodChanges = [];
    public ?string $date;
    public ?string $title;

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function addMarginPeriodChange(PeriodChangeFilter $filter): self
    {
        $this->marginPeriodChanges[] = $filter;

        return $this;
    }

    public function getFirstPeriodChange(): PeriodChangeFilter|false
    {
        return reset($this->marginPeriodChanges);
    }
}
