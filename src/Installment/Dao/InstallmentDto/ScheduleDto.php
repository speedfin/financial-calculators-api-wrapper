<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao\InstallmentDto;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class ScheduleDto extends DataTransferObject
{
    public ?int $number;
    #[CastWith(FloatCaster::class)]
    public ?float $margin;
    public ?string $date;
    public ?ValueDto $value;
}