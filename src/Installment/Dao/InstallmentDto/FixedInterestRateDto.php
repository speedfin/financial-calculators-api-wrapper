<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao\InstallmentDto;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class FixedInterestRateDto extends DataTransferObject
{
    public ?int $periodFrom;
    public ?int $periodTo;
    #[CastWith(FloatCaster::class)]
    public ?float $interestValue;
    #[CastWith(FloatCaster::class)]
    public ?float $installmentValue;
}