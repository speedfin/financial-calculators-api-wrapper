<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao\InstallmentDto;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class ValueDto extends DataTransferObject
{
    #[CastWith(FloatCaster::class)]
    public ?float $restCreditValue;
    #[CastWith(FloatCaster::class)]
    public ?float $value;
    #[CastWith(FloatCaster::class)]
    public ?float $capitalValue;
    #[CastWith(FloatCaster::class)]
    public ?float $interestValue;
}