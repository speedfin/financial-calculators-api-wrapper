<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao\InstallmentDto;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class EqualDto extends DataTransferObject
{
    #[CastWith(FloatCaster::class)]
    public ?float $total;
    #[CastWith(FloatCaster::class)]
    public ?float $monthly;
    #[CastWith(FloatCaster::class)]
    public ?float $capitalValue;
    #[CastWith(FloatCaster::class)]
    public ?float $interestValue;
    public ?FixedInterestRateDto $fixedInterestRate;
    #[CastWith(ArrayCaster::class, itemType: PeriodicDto::class)]
    public ?array $periodic;
}