<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao\InstallmentFilter;

use Spatie\DataTransferObject\DataTransferObject;

class PeriodMarginFilter extends DataTransferObject
{
    public ?int $periodFrom;
    public ?int $periodTo;
    public ?float $interestRate;
    public ?float $surchargeRate;
}