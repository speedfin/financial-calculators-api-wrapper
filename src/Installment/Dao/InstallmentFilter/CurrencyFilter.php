<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao\InstallmentFilter;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class CurrencyFilter extends DataTransferObject
{
    public ?string $symbol;
    #[CastWith(FloatCaster::class)]
    public ?float $value;
    #[CastWith(BooleanCaster::class)]
    public ?bool $showForeign;
}