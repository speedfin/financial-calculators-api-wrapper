<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao\InstallmentFilter;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;
use Speedfin\Calculators\Common\Dao\Caster\IntCaster;

class FixedInterestRateFilter extends DataTransferObject
{
    #[CastWith(IntCaster::class, default: 0)]
    public ?int $periodFrom;
    #[CastWith(IntCaster::class, default: 0)]
    public ?int $periodTo;
    #[CastWith(FloatCaster::class)]
    public ?float $value;

    public function hasValues(): bool
    {
        return $this->periodFrom && $this->periodTo && $this->value;
    }
}