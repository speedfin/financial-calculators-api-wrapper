<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dao\InstallmentFilter;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class PeriodChangeFilter extends DataTransferObject
{
    public ?int $from;
    public ?int $to;
    #[CastWith(FloatCaster::class)]
    public ?float $value;
    #[CastWith(BooleanCaster::class)]
    public ?bool $forced;
}