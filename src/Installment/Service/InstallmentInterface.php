<?php

namespace Speedfin\Calculators\Installment\Service;

use Speedfin\Calculators\Common\Service\JWTInterface;
use Speedfin\Calculators\Installment\Dao\InstallmentDto;
use Speedfin\Calculators\Installment\Dao\InstallmentFilter;

interface InstallmentInterface extends JWTInterface
{
    const INSTALLMENT_API_ENDPOINT = 'api/v2/installment';

    public function calculate(InstallmentFilter $filters): InstallmentDto;
}