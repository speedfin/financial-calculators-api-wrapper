<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Service;

use Speedfin\Calculators\Common\Service\AbstractProduct;
use Speedfin\Calculators\Installment\Adapter\InstallmentAdapter;
use Speedfin\Calculators\Installment\Dao\InstallmentDto;
use Speedfin\Calculators\Installment\Dao\InstallmentFilter;

class Installment extends AbstractProduct implements InstallmentInterface
{
    public function calculate(InstallmentFilter $filters): InstallmentDto
    {
        $installmentAdapter = new InstallmentAdapter($this->getUrl(self::INSTALLMENT_API_ENDPOINT), $this->token);

        return $installmentAdapter->get($filters);
    }
}