<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto;

final readonly class InstallmentDto
{
    public function __construct(
        public ?EqualDto $equal,
        public ?DecreasingDto $decreasing,
        /**
         * @var ScheduleDto[]|null
         */
        public ?array $equalSchedule,
        /**
         * @var ScheduleDto[]|null
         */
        public ?array $decreasingSchedule
    ) {
    }
}
