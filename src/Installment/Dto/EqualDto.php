<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto;

final readonly class EqualDto
{
    public function __construct(
        public ?float $total,
        public ?float $monthly,
        public ?float $capitalValue,
        public ?float $interestValue,
        public ?FixedInterestRateDto $fixedInterestRate,
        /**
         * @var PeriodicDto[]|null
         */
        public ?array $periodic
    ) {
    }
}
