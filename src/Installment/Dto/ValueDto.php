<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto;

final readonly class ValueDto
{
    public function __construct(
        public ?float $restCreditValue,
        public ?float $value,
        public ?float $capitalValue,
        public ?float $interestValue
    ) {
    }
}
