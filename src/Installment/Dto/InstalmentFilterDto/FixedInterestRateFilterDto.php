<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto\InstalmentFilterDto;

final readonly class FixedInterestRateFilterDto
{
    public function __construct(
        public ?int $periodFrom,
        public ?int $periodTo,
        public ?float $value
    ) {
    }

    public function hasValues(): bool
    {
        return $this->periodFrom && $this->periodTo && $this->value;
    }
}