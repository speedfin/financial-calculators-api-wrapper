<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto\InstalmentFilterDto;

final readonly class InstallmentFilterDto
{
    public function __construct(
        public ?int $creditValue,
        public ?int $creditPeriod,
        public ?float $interest,
        public ?float $currencyIndex,
        public ?float $margin,
        public ?float $surchargeRate,
        public ?float $surchargeIndexRate,
        public ?FixedInterestRateFilterDto $fixedInterestRate,
        public ?CurrencyFilterDto $currency,
        /**
         * @var PeriodMarginFilterDto[]|null
         */
        public ?array $margins,
        public ?string $interestType,
        /**
         * @var PeriodChangeFilterDto[]|null
         */
        public ?array $marginPeriodChanges,
        public ?string $date,
        public ?string $title
    ) {
    }

    public function getFirstPeriodChange(): PeriodChangeFilterDto|false
    {
        return reset($this->marginPeriodChanges);
    }
}
