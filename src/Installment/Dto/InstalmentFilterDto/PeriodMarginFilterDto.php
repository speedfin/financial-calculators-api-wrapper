<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto\InstalmentFilterDto;

final readonly class PeriodMarginFilterDto
{
    public function __construct(
        public ?int $periodFrom,
        public ?int $periodTo,
        public ?float $interestRate,
        public ?float $surchargeRate
    ) {
    }
}