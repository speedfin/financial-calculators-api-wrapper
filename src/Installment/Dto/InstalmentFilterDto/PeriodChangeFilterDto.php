<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto\InstalmentFilterDto;

final readonly class PeriodChangeFilterDto
{
    public function __construct(
        public ?int $from,
        public ?int $to,
        public ?float $value,
        public ?bool $forced
    ) {
    }
}