<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto\InstalmentFilterDto;

final readonly class CurrencyFilterDto
{
    public function __construct(
        public ?string $symbol,
        public ?float $value,
        public ?bool $showForeign
    ) {
    }
}