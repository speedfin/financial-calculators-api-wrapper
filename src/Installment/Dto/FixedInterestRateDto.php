<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto;

final readonly class FixedInterestRateDto
{
    public function __construct(
        public ?int $periodFrom,
        public ?int $periodTo,
        public ?float $interestValue,
        public ?float $installmentValue
    ) {
    }
}
