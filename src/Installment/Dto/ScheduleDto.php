<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto;

final readonly class ScheduleDto
{
    public function __construct(
        public ?int $number,
        public ?float $margin,
        public ?string $date,
        public ?ValueDto $value
    ) {
    }
}
