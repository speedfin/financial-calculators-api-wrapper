<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Dto;

final readonly class PeriodicDto
{
    public function __construct(
        public ?float $value,
        public ?float $interest
    ) {
    }
}
