<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Installment\Adapter;

use Speedfin\Calculators\Common\Adapter\AbstractAdapter;
use Speedfin\Calculators\Installment\Dao\InstallmentDto;
use Speedfin\Calculators\Installment\Dao\InstallmentFilter;

class InstallmentAdapter extends AbstractAdapter
{
    public function get(InstallmentFilter $filters): InstallmentDto
    {
        return new InstallmentDto(
            $this->sendGetRequest($filters->toArray())->toArray()
        );
    }
}