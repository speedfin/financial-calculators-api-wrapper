<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Service;

use Symfony\Component\PropertyInfo\Extractor\ConstructorExtractor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

final class Deserialize
{
    private static ?Serializer $serializer = null;

    private static function getSerializer(): Serializer
    {
        if (self::$serializer === null) {
            $phpDocExtractor = new PhpDocExtractor();
            $typeExtractor = new PropertyInfoExtractor(
                typeExtractors: [
                    new ConstructorExtractor([$phpDocExtractor]),
                    $phpDocExtractor,
                ],
            );

            $normalizers = [
                new ObjectNormalizer(propertyTypeExtractor: $typeExtractor),
                new ArrayDenormalizer(),
            ];

            $encoders = [
                new JsonEncoder(),
            ];

            self::$serializer = new Serializer($normalizers, $encoders);
        }

        return self::$serializer;
    }

    public static function deserialize(string $json, string $className): object
    {
        return self::getSerializer()->deserialize($json, $className, 'json');
    }
}
