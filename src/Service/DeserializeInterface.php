<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Service;

interface DeserializeInterface
{
    public static function deserialize(string $json, string $className): object;
}