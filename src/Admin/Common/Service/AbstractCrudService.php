<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Service;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

abstract class AbstractCrudService
{
    protected AbstractAdapter $adapter;
    protected string $token;

    public function delete(int $id): void
    {
        $this->adapter->deleteRequest($id, $this->token);
    }
    
    public function updateItem(AbstractItemDto $itemDto): array
    {
        return $this->adapter->updateRequest($itemDto->id, $itemDto->toArray(), $this->token)->toArray();
    }
    
    public function createItem(AbstractItemDto $itemDto): array
    {
        return $this->adapter->createRequest($itemDto->toArray(), $this->token)->toArray();
    }
    
    public function getOneItem(int $id): ?array
    {
        $response = $this->adapter->getOneRequest($id, $this->token);
        if ($response->getStatusCode() !== 200) return null;

        return $response->toArray();
    }
    
    public function getAllItems(AbstractFilter $filter): array
    {
        return $this->adapter->getAllRequest($filter->toArray(), $this->token)->toArray();
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }
}
