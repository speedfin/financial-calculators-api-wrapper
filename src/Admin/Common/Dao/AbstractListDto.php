<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao;

use Spatie\DataTransferObject\DataTransferObject;

abstract class AbstractListDto extends DataTransferObject
{
    public function getItems(): array
    {
        return $this->items;
    }
    public function first(): ?AbstractItemDto
    {
        return !empty($this->items) ? reset($this->items) : null;
    }
}
