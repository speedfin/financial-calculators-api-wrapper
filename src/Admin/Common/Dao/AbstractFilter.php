<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Dao;

use Spatie\DataTransferObject\DataTransferObject;

abstract class AbstractFilter extends DataTransferObject
{
    public int $itemsPerPage = 100000;
    public ?array $exists;
}
