<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Common\Adapter;

use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response\CurlResponse;

abstract class AbstractAdapter
{
    public string $createAPIEndpoint;
    public string $updateAPIEndpoint;
    public string $deleteAPIEndpoint;
    public string $getOneAPIEndpoint;
    public string $getAllAPIEndpoint;
    public CurlHttpClient $client;

    public function __construct(
        string $createAPIEndpoint,
        string $updateAPIEndpoint,
        string $deleteAPIEndpoint,
        string $getOneAPIEndpoint,
        string $getAllAPIEndpoint
    ) {
        $this->createAPIEndpoint = $createAPIEndpoint;
        $this->updateAPIEndpoint = $updateAPIEndpoint;
        $this->deleteAPIEndpoint = $deleteAPIEndpoint;
        $this->getOneAPIEndpoint = $getOneAPIEndpoint;
        $this->getAllAPIEndpoint = $getAllAPIEndpoint;
        $this->client = HttpClient::create();
    }

    public function createRequest(array $data, string $token): CurlResponse
    {
        return $this->client->request(
            'POST',
            $this->createAPIEndpoint,
            [
                'json' => $data,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json'
                ]
            ]
        );
    }

    public function updateRequest(int $id, array $data, string $token): CurlResponse
    {
        return $this->client->request(
            'PATCH',
            str_replace('{id}', (string) $id, $this->updateAPIEndpoint),
            [
                'json' => $data,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-type' => 'application/merge-patch+json'
                ]
            ]
        );
    }

    public function deleteRequest(int $id, string $token): CurlResponse
    {
        return $this->client->request(
            'DELETE',
            str_replace('{id}', (string) $id, $this->deleteAPIEndpoint),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]
        );
    }

    public function getOneRequest(int $id, string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            str_replace('{id}', (string) $id, $this->getOneAPIEndpoint),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }

    public function getAllRequest(array $filters, string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            $this->getAllAPIEndpoint,
            [
                'query' => $filters,
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }

    public static function getGetOneEndpointIRI(int|string $id): string
    {
        return str_replace('{id}', (string) $id, static::GET_ONE_ENDPOINT);
    }
}
