<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\CompanyLoan\Dao\CompanyLoanCorrection;

use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionFieldsDto;

class CompanyLoanCorrectionFieldsDto extends AbstractProductCorrectionFieldsDto
{

}