<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoanCorrection;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class MoneyLoanCorrectionAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/money_loan_corrections/{id}';
    public string $apiDomain;

    public function __construct(string $apiDomain)
    {
        $this->apiDomain = $apiDomain;
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/money_loan_corrections'),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_corrections/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_corrections/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/money_loan_corrections')
        );
    }

    public function getFields(string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            sprintf('%s%s', $this->apiDomain, '/admin/money_loan_corrections/fields/collection'),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }
}
