<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Adapter\MoneyLoan;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class MoneyLoanAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/money_loans/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/money_loans'),
            sprintf('%s%s', $apiDomain, '/admin/money_loans/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/money_loans/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/money_loans')
        );
    }
}
