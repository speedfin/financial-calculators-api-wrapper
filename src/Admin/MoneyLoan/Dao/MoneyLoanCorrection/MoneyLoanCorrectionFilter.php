<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MoneyLoanCorrectionFilter extends AbstractFilter
{
    public ?int $moneyLoan;
    public ?bool $required;
    public ?bool $active;
    public ?int $firm;
}