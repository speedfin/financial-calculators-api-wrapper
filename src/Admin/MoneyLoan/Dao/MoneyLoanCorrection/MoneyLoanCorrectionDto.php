<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionDto;

class MoneyLoanCorrectionDto extends AbstractProductCorrectionDto
{
    public ?string $moneyLoan;
    public ?string $type = "life_insurance";
    public ?string $paymentType;
    public ?string $paymentBase;
    public ?bool $creditAfterCheck;
}
