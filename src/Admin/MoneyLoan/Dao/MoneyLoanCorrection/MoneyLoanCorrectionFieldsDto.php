<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\MoneyLoan\Dao\MoneyLoanCorrection;

use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionFieldsDto;

class MoneyLoanCorrectionFieldsDto extends AbstractProductCorrectionFieldsDto
{

}
