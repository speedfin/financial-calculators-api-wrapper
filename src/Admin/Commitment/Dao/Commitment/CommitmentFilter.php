<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Commitment\Dao\Commitment;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class CommitmentFilter extends AbstractFilter
{
    public ?bool $visible;
}
