<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\AbilityModule;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Admin\Ability\Dao\AbilityModule\AbilityModuleFields\TypeDto;

class AbilityModuleFieldsDto extends DataTransferObject
{
    public array $periods;
    public array $regions;
    public array $realEstates;
    #[CastWith(ArrayCaster::class, itemType: TypeDto::class)]
    public array $types;
    
    public function getVariableByType(string $type): ?string
    {
        return $this->getByType($type, 'variable');
    }

    public function getLabelByType(string $type): ?string
    {
        return $this->getByType($type, 'label');
    }

    public function getByType(string $type, string $field): ?string
    {
        /** @var TypeDto $typeDto */
        foreach ($this->types as $typeDto) {
            if ($typeDto->type === $type) {
                return $typeDto->$field;
            }
        }

        return null;
    }
}