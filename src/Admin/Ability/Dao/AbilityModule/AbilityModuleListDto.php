<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Dao\AbilityModule;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class AbilityModuleListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: AbilityModuleDto::class)]
    public ?array $items = [];
}
