<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Ability\Adapter\AbilityModule;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class AbilityModuleAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/ability_modules/{id}';
    public string $apiDomain;

    public function __construct(string $apiDomain)
    {
        $this->apiDomain = $apiDomain;
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/ability_modules'),
            sprintf('%s%s', $apiDomain, '/admin/ability_modules/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/ability_modules/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/ability_modules')
        );
    }

    public function getFields(string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            sprintf('%s%s', $this->apiDomain, '/admin/ability_module/fields/collection'),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }
}
