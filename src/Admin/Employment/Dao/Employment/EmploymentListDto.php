<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Employment\Dao\Employment;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractListDto;

class EmploymentListDto extends AbstractListDto
{
    #[CastWith(ArrayCaster::class, itemType: EmploymentDto::class)]
    public ?array $items = [];
}
