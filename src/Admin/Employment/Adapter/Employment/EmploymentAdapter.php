<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Employment\Adapter\Employment;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class EmploymentAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/employments/{id}';
    public string $apiDomain;
    
    public function __construct(string $apiDomain)
    {
        $this->apiDomain = $apiDomain;
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/employments'),
            sprintf('%s%s', $apiDomain, '/admin/employments/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/employments/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/employments')
        );
    }

    public function getFields(string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            sprintf('%s%s', $this->apiDomain, '/admin/employments/fields/collection'),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }
}
