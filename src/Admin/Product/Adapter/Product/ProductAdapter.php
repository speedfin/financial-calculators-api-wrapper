<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Product\Adapter\Product;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class ProductAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/products/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/products'),
            sprintf('%s%s', $apiDomain, '/admin/products/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/products/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/simple_products')
        );
    }
}
