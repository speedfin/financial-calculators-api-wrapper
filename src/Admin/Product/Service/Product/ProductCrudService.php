<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Product\Service\Product;

use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Product\Adapter\Product\ProductAdapter;
use Speedfin\Calculators\Admin\Product\Dao\Product\ProductFilter;
use Speedfin\Calculators\Admin\Product\Dao\Product\ProductListDto;

class ProductCrudService extends AbstractCrudService implements ProductCrudServiceInterface
{
    public function __construct(ProductAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    public function getAll(ProductFilter $filter): ProductListDto
    {
        return new ProductListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}
