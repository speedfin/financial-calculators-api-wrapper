<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Product\Dao\Product;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\AbstractItemDto;

class ProductDto extends AbstractItemDto
{
    public string $name;
    public ?int $bankId;
}
