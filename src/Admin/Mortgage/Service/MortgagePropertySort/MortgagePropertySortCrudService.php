<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgagePropertySort;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Mortgage\Adapter\MortgagePropertySort\MortgagePropertySortAdapter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort\MortgagePropertySortDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort\MortgagePropertySortFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgagePropertySort\MortgagePropertySortListDto;

class MortgagePropertySortCrudService extends AbstractCrudService implements MortgagePropertySortCrudServiceInterface
{
    public function __construct(
        MortgagePropertySortAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MortgagePropertySortDto $itemDto): MortgagePropertySortDto
    {
        return new MortgagePropertySortDto($this->createItem($itemDto));
    }

    public function update(MortgagePropertySortDto $itemDto): MortgagePropertySortDto
    {
        return new MortgagePropertySortDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MortgagePropertySortDto
    {
        return new MortgagePropertySortDto($this->getOneItem($id));
    }

    public function getAll(MortgagePropertySortFilter $filter): MortgagePropertySortListDto
    {
        return new MortgagePropertySortListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}