<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageCreditPurpose;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageCreditPurpose\MortgageCreditPurposeAdapter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose\MortgageCreditPurposeDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose\MortgageCreditPurposeFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCreditPurpose\MortgageCreditPurposeListDto;

class MortgageCreditPurposeCrudService extends AbstractCrudService implements MortgageCreditPurposeCrudServiceInterface
{
    public function __construct(
        MortgageCreditPurposeAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MortgageCreditPurposeDto $itemDto): MortgageCreditPurposeDto
    {
        return new MortgageCreditPurposeDto($this->createItem($itemDto));
    }

    public function update(MortgageCreditPurposeDto $itemDto): MortgageCreditPurposeDto
    {
        return new MortgageCreditPurposeDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MortgageCreditPurposeDto
    {
        return new MortgageCreditPurposeDto($this->getOneItem($id));
    }

    public function getAll(MortgageCreditPurposeFilter $filter): MortgageCreditPurposeListDto
    {
        return new MortgageCreditPurposeListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }
}