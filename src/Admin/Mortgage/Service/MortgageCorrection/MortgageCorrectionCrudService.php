<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Service\MortgageCorrection;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageCorrection\MortgageCorrectionAdapter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionFieldsDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection\MortgageCorrectionListDto;

class MortgageCorrectionCrudService extends AbstractCrudService implements MortgageCorrectionCrudServiceInterface
{
    public function __construct(
        MortgageCorrectionAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MortgageCorrectionDto $itemDto): MortgageCorrectionDto
    {
        return new MortgageCorrectionDto($this->createItem($itemDto));
    }

    public function update(MortgageCorrectionDto $itemDto): MortgageCorrectionDto
    {
        return new MortgageCorrectionDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MortgageCorrectionDto
    {
        return new MortgageCorrectionDto($this->getOneItem($id));
    }

    public function getAll(MortgageCorrectionFilter $filter): MortgageCorrectionListDto
    {
        return new MortgageCorrectionListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function getFields(): MortgageCorrectionFieldsDto
    {
        return new MortgageCorrectionFieldsDto($this->adapter->getFields($this->token)->toArray());
    }
}