<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Service\Mortgage;

use Speedfin\Calculators\Admin\Bank\Adapter\Bank\BankAdapter;
use Speedfin\Calculators\Admin\Bank\Dao\Bank\BankDto;
use Speedfin\Calculators\Admin\Bank\Service\Bank\BankCrudServiceInterface;
use Speedfin\Calculators\Admin\Common\Service\AbstractCrudService;
use Speedfin\Calculators\Admin\Mortgage\Adapter\Mortgage\MortgageAdapter;
use Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage\MortgageDto;
use Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage\MortgageFilter;
use Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage\MortgageListDto;

class MortgageCrudService extends AbstractCrudService implements MortgageCrudServiceInterface
{
    public function __construct(
        MortgageAdapter $adapter
    )
    {
        $this->adapter = $adapter;
    }

    public function create(MortgageDto $itemDto): MortgageDto
    {
        $this->convertDateTimeToStringBeforeSend($itemDto);
        return new MortgageDto($this->createItem($itemDto));
    }

    public function update(MortgageDto $itemDto): MortgageDto
    {
        $this->convertDateTimeToStringBeforeSend($itemDto);
        return new MortgageDto($this->updateItem($itemDto));
    }

    public function getOne(int $id): MortgageDto
    {
        return new MortgageDto($this->getOneItem($id));
    }

    public function getAll(MortgageFilter $filter): MortgageListDto
    {
        return new MortgageListDto([
            'items' => $this->getAllItems($filter)
        ]);
    }

    public function convertDateTimeToStringBeforeSend(MortgageDto $itemDto)
    {
        $itemDto->promotionDateFrom = $itemDto->promotionDateFrom instanceof \DateTime ?
            $itemDto->promotionDateFrom->format('Y-m-d') :
            $itemDto->promotionDateFrom;
        $itemDto->promotionDateTo = $itemDto->promotionDateTo instanceof \DateTime ?
            $itemDto->promotionDateTo->format('Y-m-d') :
            $itemDto->promotionDateTo ;
    }
}