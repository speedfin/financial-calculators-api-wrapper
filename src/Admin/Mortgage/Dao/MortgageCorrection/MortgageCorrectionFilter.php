<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MortgageCorrectionFilter extends AbstractFilter
{
    public ?int $mortgage;
    public ?int $firm;

    public function setMortgageId(?int $mortgageId): self
    {
        $this->mortgage = $mortgageId;

        return $this;
    }
}