<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\MortgageCorrection;

use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductCorrectionFieldsDto;

class MortgageCorrectionFieldsDto extends AbstractProductCorrectionFieldsDto
{

}