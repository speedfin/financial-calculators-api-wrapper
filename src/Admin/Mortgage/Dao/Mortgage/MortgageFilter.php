<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage;

use Speedfin\Calculators\Admin\Common\Dao\AbstractFilter;

class MortgageFilter extends AbstractFilter
{
    public bool $archived = false;
    public ?bool $loan;

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function setLoan(bool $loan): self
    {
        $this->loan = $loan;

        return $this;
    }
}