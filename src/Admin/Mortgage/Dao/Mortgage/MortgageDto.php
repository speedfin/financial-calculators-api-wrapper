<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Dao\Mortgage;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Speedfin\Calculators\Admin\Common\Dao\Product\AbstractProductDto;
use Speedfin\Calculators\Common\Enum\InterestType;

class MortgageDto extends AbstractProductDto
{
    const INTEREST_TYPES = [
        InterestType::VARIABLE_INTEREST_RATE_TYPE->value => 'zmienne',
        InterestType::FIXED_INTEREST_RATE_TYPE->value => 'stałe',
        InterestType::PERIODICAL_INTEREST_RATE_TYPE->value => 'okresowe',
    ];

    public ?bool $loan;
    public string $interestType = 'fixed_interest_rate';
    public string $relatedType = 'Mortgage';
    public ?bool $forceProvisionCredited;
    public bool $checkProvisionCredited = false;
    public ?int $constructionPeriod;
    public ?array $mortgageCreditPurpose = [];
    public ?array $mortgagePropertySorts = [];
    public ?bool $eco = false;
    public bool $equalInstallmentEnabled = true;
    public bool $decreasingInstallmentEnabled = true;
    public ?string $surchangeIndex;
    public ?CurrencyIndexDto $surchangeIndexItem;
    public ?string $indexType;

    public function isFixedInterestRate()
    {
        return $this->interestType === InterestType::FIXED_INTEREST_RATE_TYPE->value;
    }

    public function markAsLoan()
    {
        $this->loan = true;
    }
}
