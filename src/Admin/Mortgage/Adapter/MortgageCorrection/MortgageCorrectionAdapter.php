<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Adapter\MortgageCorrection;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;
use Symfony\Component\HttpClient\Response\CurlResponse;

class MortgageCorrectionAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/mortgage_corrections/{id}';
    public string $apiDomain;

    public function __construct(string $apiDomain)
    {
        $this->apiDomain = $apiDomain;
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/mortgage_corrections'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_corrections/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_corrections/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/mortgage_corrections')
        );
    }

    public function getFields(string $token): CurlResponse
    {
        return $this->client->request(
            'GET',
            sprintf('%s%s', $this->apiDomain, '/admin/mortgage_corrections/fields/collection'),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }
}