<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Admin\Mortgage\Adapter\Mortgage;

use Speedfin\Calculators\Admin\Common\Adapter\AbstractAdapter;

class MortgageAdapter extends AbstractAdapter
{
    const GET_ONE_ENDPOINT = '/admin/mortgages/{id}';

    public function __construct(string $apiDomain)
    {
        parent::__construct(
            sprintf('%s%s', $apiDomain, '/admin/mortgages'),
            sprintf('%s%s', $apiDomain, '/admin/mortgages/{id}'),
            sprintf('%s%s', $apiDomain, '/admin/mortgages/{id}'),
            sprintf('%s%s', $apiDomain, self::GET_ONE_ENDPOINT),
            sprintf('%s%s', $apiDomain, '/admin/mortgages')
        );
    }
}