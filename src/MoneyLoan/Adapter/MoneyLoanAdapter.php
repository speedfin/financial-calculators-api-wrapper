<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Adapter;

use Speedfin\Calculators\Common\Adapter\AbstractAdapter;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\MoneyLoanArchiveFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\MoneyLoanFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanListDto;
use Speedfin\Calculators\Service\Deserialize;

class MoneyLoanAdapter extends AbstractAdapter
{
    public function getCollection(MoneyLoanFilterDto $filters): MoneyLoanListDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), MoneyLoanListDto::class);
    }

    public function getItem(MoneyLoanFilterDto $filters): MoneyLoanDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), MoneyLoanDto::class);
    }

    public function getArchiveCollection(MoneyLoanArchiveFilterDto $filters): MoneyLoanListDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), MoneyLoanListDto::class);
    }
}