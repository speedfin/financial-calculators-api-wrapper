<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

class GroupedFilterDto
{
    public function __construct(
        public ?int $markedType
    ) {
    }
}
