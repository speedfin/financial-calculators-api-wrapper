<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class RrsoDto
{
    public function __construct(
        public ?float $value
    ) {
    }
}
