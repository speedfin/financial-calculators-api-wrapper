<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class CostDto
{
    public function __construct(
        public ?float $creditValue,
        public ?float $secureValue,
        public ?float $creditValueGross,
        public ?float $initial,
        public ?float $total,
        public ?float $valuation = 0.00
    ) {
    }
}
