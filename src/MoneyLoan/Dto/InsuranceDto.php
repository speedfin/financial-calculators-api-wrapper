<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class InsuranceDto
{
    public function __construct(
        /**
         * @var CorrectionDto[]
         */
        public ?array $lifeInsurance,
        public ?string $description
    ) {
    }
}
