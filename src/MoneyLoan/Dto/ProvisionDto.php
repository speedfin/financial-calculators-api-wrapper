<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class ProvisionDto
{
    public function __construct(
        public ?float $percent,
        public mixed $value,
        public ?float $minValue,
        public ?float $maxValue,
        public ?bool $allowCrediting,
        public ?bool $credited,
    ) {
    }
}
