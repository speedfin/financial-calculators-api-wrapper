<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class InfoFieldsDto
{
    public function __construct(
        public ?string $interest,
        public  ?ProvisionDto $provision
    ) {
    }
}
