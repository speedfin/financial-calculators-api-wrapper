<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class InterestDto
{
    public function __construct(
        public ?float $value,
        public ?string $type,
    ) {
    }
}
