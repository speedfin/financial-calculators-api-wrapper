<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class LogoDto
{
    public function __construct(
        public ?string $small,
        public ?string $medium,
        public ?string $large
    ) {
    }
}
