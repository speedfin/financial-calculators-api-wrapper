<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class PromotionDto
{
    public function __construct(
        public ?string $dateFrom,
        public ?string $dateTo,
        public ?string $name
    ) {
    }
}
