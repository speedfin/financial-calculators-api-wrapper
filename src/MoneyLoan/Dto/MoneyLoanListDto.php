<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

class MoneyLoanListDto
{
    public function __construct(
        /**
         * @var MoneyLoanDto[]
         */
        public ?array $items
    ) {
    }
}