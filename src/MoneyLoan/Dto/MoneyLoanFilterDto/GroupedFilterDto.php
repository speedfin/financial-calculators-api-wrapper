<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto;

final readonly class GroupedFilterDto
{
    public function __construct(
        public ?bool $consolidationOffer,
        public ?bool $internalClient,
        public ?bool $externalClient,
        public ?bool $promotion,
        public ?bool $freelance,
        public ?bool $uniformedServices,
        public ?bool $retirementDisability,
        public ?bool $incomeFromAbroad,
        public ?bool $contractOfEmployment,
        public ?bool $commissionWorkContract,
        public ?bool $economicActivity
    ) {
    }
}
