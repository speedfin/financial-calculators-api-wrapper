<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto;

use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\ItemFilterDto\ItemFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\SliceAndSortableFilterDto\SliceFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\SliceAndSortableFilterDto\SortableFilterDto;

final readonly class MoneyLoanFilterDto
{
    public function __construct(
        public ?SliceFilterDto $slice,
        public ?SortableFilterDto $sortable,
        public ?array $banks,
        public ?array $ids,
        public ?int $firmId,
        public ?bool $clientInternal,
        public ?bool $clientExternal,
        public ?bool $insuranceProducts,
        public ?bool $insuranceProductsHide,
        public ?bool $markedMode,
        public ?bool $fixedInterestRate,
        public ?bool $variableInterestRate,
        public ?GroupedFilterDto $grouped,
        public ?ItemFilterDto $item,
        public ?int $creditClientAge = 30,
        public ?int $creditValue = 50000,
        public ?int $creditPeriod = 60
    ) {
    }
}
