<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\ItemFilterDto;

final readonly class ExpertValueFilterDto
{
    public function __construct(
        public ?int $otherCost,
        public ?string $comment
    ) {
    }
}
