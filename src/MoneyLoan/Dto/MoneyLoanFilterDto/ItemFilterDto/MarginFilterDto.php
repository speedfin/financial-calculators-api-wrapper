<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\ItemFilterDto;

final readonly class MarginFilterDto
{
    public function __construct(
        public ?float $value,
        public ?float $valueManual
    ) {
    }
}
