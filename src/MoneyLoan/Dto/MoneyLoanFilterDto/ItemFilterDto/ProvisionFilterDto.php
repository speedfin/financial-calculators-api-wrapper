<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\ItemFilterDto;

final readonly class ProvisionFilterDto
{
    public function __construct(
        public ?float $percent,
        public ?float $percentManual,
        public ?bool $credited
    ) {
    }
}
