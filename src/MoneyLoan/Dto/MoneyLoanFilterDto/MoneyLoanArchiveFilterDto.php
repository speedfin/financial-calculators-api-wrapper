<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto;

final readonly class MoneyLoanArchiveFilterDto
{
    public function __construct(
        /**
         * @var MoneyLoanFilterDto[]
         */
        public ?array $filters
    ) {
    }
}