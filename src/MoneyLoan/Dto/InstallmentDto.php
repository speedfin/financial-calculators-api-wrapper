<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class InstallmentDto
{
    public function __construct(
        public ?EqualDto $equal,
        public ?DecreasingDto $decreasing,
    ) {
    }
}
