<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class MoneyLoanDto
{
    public function __construct(
        public ?int $id,
        public ?int $cloneId,
        public ?string $requestId,
        public ?string $title,
        public ?string $description,
        public ?RrsoDto $rrso,
        public ?CreditPeriodDto $creditPeriod,
        public ?RequirementDto $requirement,
        public ?ProvisionDto $provision,
        public ?GroupedFilterDto $groupedFilter,
        public ?CostDto $cost,
        public ?ProsConsDto $prosCons,
        public ?InsuranceDto $insurance,
        public ?PromotionDto $promotion,
        public ?MarginDto $margin,
        public ?InterestDto $interest,
        public ?CorrectionImpactDto $correctionImpact,
        public ?InstallmentDto $installment,
        public ?BankDto $bank,
        public ?ExpertValueDto $expertValue,
        public ?InfoFieldsDto $infoFields
    ) {
    }
}
