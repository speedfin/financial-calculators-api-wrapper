<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class MarginDto
{
    public function __construct(
        public mixed $value,
        public ?float $maxValue,
        public ?float $minValue
    ) {
    }
}
