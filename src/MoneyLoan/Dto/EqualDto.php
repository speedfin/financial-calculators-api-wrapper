<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class EqualDto
{
    public function __construct(
        public mixed $monthly,
        public mixed $monthlyTotal,
        public mixed $total,
        public mixed $interestValue,
    ) {
    }
}
