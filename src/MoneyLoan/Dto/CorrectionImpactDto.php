<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Dto;

final readonly class CorrectionImpactDto
{
    public function __construct(
        public ?bool $creditingPossibilityExceeded,
        public ?bool $forceCreditingPossibility,
        public ?float $manualMarginFromCheckNotRequired,
        public ?float $manualProvisionFromCheckNotRequired
    ) {
    }
}
