<?php

namespace Speedfin\Calculators\MoneyLoan\Service;

use Speedfin\Calculators\Common\Service\JWTInterface;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\MoneyLoanArchiveFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\MoneyLoanFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanListDto;

interface MoneyLoanInterface extends JWTInterface
{
    const LIST_API_ENDPOINT = 'api/v3/money_loan';
    const LIST_ARCHIVE_API_ENDPOINT = 'api/v3/money_loan/archive';
    const ITEM_API_ENDPOINT = 'api/v2/money_loan/{id}';

    public function calculate(MoneyLoanFilterDto $filter): MoneyLoanListDto;

    public function calculateItem(int $id, MoneyLoanFilterDto $filter): MoneyLoanDto;

    public function calculateArchive(MoneyLoanArchiveFilterDto $filter): MoneyLoanListDto;

    public function setCache(bool $cache): void;
}