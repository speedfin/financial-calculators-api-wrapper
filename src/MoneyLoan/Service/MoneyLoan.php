<?php

declare(strict_types=1);

namespace Speedfin\Calculators\MoneyLoan\Service;

use Speedfin\Calculators\Common\Service\AbstractProduct;
use Speedfin\Calculators\MoneyLoan\Adapter\MoneyLoanAdapter;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\MoneyLoanArchiveFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\MoneyLoanFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanListDto;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class MoneyLoan extends AbstractProduct implements MoneyLoanInterface
{
    public function calculate(MoneyLoanFilterDto $filter): MoneyLoanListDto
    {
        $moneyLoanAdapter = new MoneyLoanAdapter($this->getUrl(self::LIST_API_ENDPOINT), $this->token);
        if ($this->cache === false || !$filter->firmId) {
            return $moneyLoanAdapter->getCollection($filter);
        }
        $filesystemAdapter = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );
        return $filesystemAdapter->get($this->getCacheKey($filter, sprintf('money-loan-list-%s', md5(json_encode((array) $filter)))),
            function (ItemInterface $item) use ($moneyLoanAdapter, $filter) {
                return $moneyLoanAdapter->getCollection($filter);
        });
    }

    public function calculateItem(int $id, MoneyLoanFilterDto $filter): MoneyLoanDto
    {
        $moneyLoanAdapter = new MoneyLoanAdapter(
            str_replace('{id}', (string)$id, $this->getUrl(self::ITEM_API_ENDPOINT)),
            $this->token
        );
        if ($this->cache === false) {
            return $moneyLoanAdapter->getItem($filter);
        }
        $filesystemAdapter = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );
        return $filesystemAdapter->get($this->getCacheKey($filter, sprintf('money-loan-item-%s-%s', (string) $id), md5(json_encode((array) $filter))),
            function (ItemInterface $item) use ($moneyLoanAdapter, $filter) {
                return $moneyLoanAdapter->getItem($filter);
            });
    }

    public function calculateArchive(MoneyLoanArchiveFilterDto $moneyLoanArchiveFilter): MoneyLoanListDto
    {
        $moneyLoanAdapter = new MoneyLoanAdapter(
            $this->getUrl(self::LIST_ARCHIVE_API_ENDPOINT),
            $this->token
        );

        return $moneyLoanAdapter->getArchiveCollection($moneyLoanArchiveFilter);
    }
}