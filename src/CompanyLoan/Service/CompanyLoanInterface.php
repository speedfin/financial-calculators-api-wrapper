<?php

namespace Speedfin\Calculators\CompanyLoan\Service;

use Speedfin\Calculators\Common\Service\JWTInterface;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CollectionFilterDto\CollectionFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CompanyLoanArchiveFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CompanyLoanFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanListDto;

interface CompanyLoanInterface extends JWTInterface
{
    const LIST_API_ENDPOINT = 'api/v3/company_loan';
    const LIST_ARCHIVE_API_ENDPOINT = 'api/v3/company_loan/archive';
    const ITEM_API_ENDPOINT = 'api/v2/company_loan/{id}';

    const FILTER_ECONOMIC_ACTIVITY_API_ENDPOINT = 'api/v2/company_loan/filter/economic_activity';
    const FILTER_VARIOUS_API_ENDPOINT = 'api/v2/company_loan/filter/various';
    const FILTER_TAXATION_FORM_API_ENDPOINT = 'api/v2/company_loan/filter/taxation_form';
    const FILTER_PRODUCT_TYPE_API_ENDPOINT = 'api/v2/company_loan/filter/product_type';
    const FILTER_SECURITY_API_ENDPOINT = 'api/v2/company_loan/filter/security';
    const FILTER_SECURITY_TYPE_API_ENDPOINT = 'api/v2/company_loan/filter/security_type';

    public function calculate(CompanyLoanFilterDto $filter): CompanyLoanListDto;

    public function calculateItem(int $id, CompanyLoanFilterDto $filter): CompanyLoanDto;

    public function calculateArchive(CompanyLoanArchiveFilterDto $filter): CompanyLoanListDto;

    public function getFilters(): CollectionFilterDto;

    public function setCache(bool $cache): void;
}