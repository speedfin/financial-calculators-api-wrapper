<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Service;

use Speedfin\Calculators\Common\Service\AbstractProduct;
use Speedfin\Calculators\CompanyLoan\Adapter\CompanyLoanAdapter;
use Speedfin\Calculators\CompanyLoan\Adapter\CompanyLoanCollectionFilterAdapter;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CollectionFilterDto\CollectionFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CompanyLoanArchiveFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CompanyLoanFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanListDto;
use Speedfin\Calculators\Service\Deserialize;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class CompanyLoan extends AbstractProduct implements CompanyLoanInterface
{
    public function calculate(CompanyLoanFilterDto $filter): CompanyLoanListDto
    {
        $companyLoanAdapter = new CompanyLoanAdapter(
            $this->getUrl(self::LIST_API_ENDPOINT),
            $this->token
        );
        if ($this->cache === false || !$filter->firmId) {
            return $companyLoanAdapter->getCollection($filter);
        }

        $filesystemAdapter = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );

        return $filesystemAdapter->get($this->getCacheKey($filter, sprintf('company-loan-list-%s', md5(json_encode((array) $filter)))),
            function (ItemInterface $item) use ($companyLoanAdapter, $filter) {
                return $companyLoanAdapter->getCollection($filter);
        });
    }

    public function calculateItem(int $id, CompanyLoanFilterDto $filter): CompanyLoanDto
    {
        $companyLoanAdapter = new CompanyLoanAdapter(
            str_replace('{id}', (string)$id, $this->getUrl(self::ITEM_API_ENDPOINT)),
            $this->token
        );

        if ($this->cache === false) {
            return $companyLoanAdapter->getItem($filter);
        }

        $filesystemAdapter = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );
        return $filesystemAdapter->get($this->getCacheKey($filter, sprintf('company-loan-item-%s-%s', (string) $id, md5(json_decode((array) $filter)))),
            function (ItemInterface $item) use ($companyLoanAdapter, $filter) {
                return $companyLoanAdapter->getItem($filter);
            });
    }

    public function calculateArchive(CompanyLoanArchiveFilterDto $filter): CompanyLoanListDto
    {
        $CompanyLoanAdapter = new CompanyLoanAdapter(
            $this->getUrl(self::LIST_ARCHIVE_API_ENDPOINT),
            $this->token
        );

        return $CompanyLoanAdapter->getArchiveCollection($filter);
    }

    public function getFilters(): CollectionFilterDto
    {
        $cache = new FilesystemAdapter(
            '',
            86400,
            $this->cacheDir
        );

        $token = $this->token;
        return $cache->get('company-loan-filters', function (ItemInterface $item) use ($token) {
            $variousAdapter = new CompanyLoanCollectionFilterAdapter(
                $this->getUrl(self::FILTER_VARIOUS_API_ENDPOINT),
                $token
            );
            $economicActivityAdapter = new CompanyLoanCollectionFilterAdapter(
                $this->getUrl(self::FILTER_ECONOMIC_ACTIVITY_API_ENDPOINT),
                $token
            );
            $taxationFormAdapter = new CompanyLoanCollectionFilterAdapter(
                $this->getUrl(self::FILTER_TAXATION_FORM_API_ENDPOINT),
                $token
            );
            $productTypeAdapter = new CompanyLoanCollectionFilterAdapter(
                $this->getUrl(self::FILTER_PRODUCT_TYPE_API_ENDPOINT),
                $token
            );
            $securityAdapter = new CompanyLoanCollectionFilterAdapter(
                $this->getUrl(self::FILTER_SECURITY_API_ENDPOINT),
                $token
            );
            $securityTypeAdapter = new CompanyLoanCollectionFilterAdapter(
                $this->getUrl(self::FILTER_SECURITY_TYPE_API_ENDPOINT),
                $token
            );

            $data = [
                'economicActivities' => $economicActivityAdapter->getFilter(),
                'taxationForms' => $taxationFormAdapter->getFilter(),
                'productTypes' => $productTypeAdapter->getFilter(),
                'securities' => $securityAdapter->getFilter(),
                'securityTypes' => $securityTypeAdapter->getFilter(),
                'various' => $variousAdapter->getFilter(),
            ];

            return Deserialize::deserialize(json_encode($data), CollectionFilterDto::class);
        });
    }
}
