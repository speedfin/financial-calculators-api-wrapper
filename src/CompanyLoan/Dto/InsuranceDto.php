<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

use Speedfin\Calculators\CompanyLoan\Dto\CorrectionDto;

final readonly class InsuranceDto
{
    public function __construct(
        /**
         * @var CorrectionDto[]
         */
        public ?array $administrativeFee,
        /**
         * @var CorrectionDto[]
         */
        public ?array $deMinimisGuarantee,
        /**
         * @var CorrectionDto[]
         */
        public ?array $otherWarranties,
        /**
         * @var CorrectionDto[]
         */
        public ?array $insurances,
        /**
         * @var CorrectionDto[]
         */
        public ?array $realEstateAppraisal,
        /**
         * @var CorrectionDto[]
         */
        public ?array $others,
    ) {
    }
}
