<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

use DateTimeInterface;

final readonly class PromotionDto
{
    public function __construct(
        public DateTimeInterface|null|string $dateFrom,
        public DateTimeInterface|null|string $dateTo,
        public ?string $name,
        public ?bool $specialOffer
    ) {
    }
}
