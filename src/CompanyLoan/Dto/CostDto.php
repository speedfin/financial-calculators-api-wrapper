<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class CostDto
{
    public function __construct(
        public ?float $creditValue,
        public ?float $secureValue,
        public ?float $creditValueGross,
        public ?float $initial,
        public ?float $total,
        public ?float $totalWithPropertyInsurance,
        public ?float $valuation = 0.00,
        public ?float $costPerStandby,
        public ?float $costForRenewal,
        public ?float $periodForRenewal,
        public ?int $earlyRepaymentPeriod,
        public ?float $earlyRepaymentCost,
        public ?float $commissionForConsiderationAmount,
        public ?float $commissionForConsideration,
        public ?float $commissionForCommitment,
        public ?bool $secured
    ) {
    }
}
