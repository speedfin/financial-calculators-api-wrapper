<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class CompanyLoanListDto
{
    public function __construct(
        /**
         * @var CompanyLoanDto[]
         */
        public ?array $items
    ) {
    }
}
