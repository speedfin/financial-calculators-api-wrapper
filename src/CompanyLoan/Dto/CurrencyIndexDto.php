<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class CurrencyIndexDto
{
    public function __construct(
        public ?int $id,
        public ?float $value
    ) {
    }
}
