<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class MarginDto
{
    public function __construct(
        public mixed $value,
        public ?float $valueManual
    ) {
    }
}
