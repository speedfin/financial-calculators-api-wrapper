<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class InfoFieldsDto
{
    public function __construct(
        public ?string $margin,
        public ?string $costPerStandby,
        public ?string $costForRenewal,
        public ?string $earlyRepaymentCost,
        public ?string $commissionForConsideration,
        public ?string $commissionForCommitment,
        public ?string $gracePeriod,
        public ?string $secureValue,
        public ?string $ltv,
        public ?string $creditPeriod,
        public ?string $provision
    ) {
    }
}
