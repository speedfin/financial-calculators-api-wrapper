<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class CreditValueDto
{
    public function __construct(
        public ?int $maxValue,
        public ?int $minValue
    ) {
    }
}
