<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class PeriodChangeDto
{
    public function __construct(
        public ?int $period,
        public ?float $value,
        public ?float $interest,
        public ?float $valueAfterPeriodChange
    ) {
    }
}
