<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class EqualDto
{
    public function __construct(
        public mixed $monthly,
        public mixed $monthlyTotal,
        public mixed $total,
        public ?bool $enabled,
        public ?FixedInterestRateInstallmentDto $fixedInterestRate,
        public mixed $interestValue,
        public ?PeriodChangeDto $periodChange
    ) {
    }
}
