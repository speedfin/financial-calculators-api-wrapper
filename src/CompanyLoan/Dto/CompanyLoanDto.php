<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class CompanyLoanDto
{
    public function __construct(
        public ?int $id,
        public ?int $cloneId,
        public ?string $requestId,
        public ?string $title,
        public ?string $description,
        public ?MarginDto $margin,
        public ?CreditPeriodDto $creditPeriod,
        public ?CreditValueDto $creditValue,
        public ?LtvDto $ltv,
        public ?LtcDto $ltc,
        public ?ProvisionDto $provision,
        public ?CurrencyDto $currency,
        public ?CostDto $cost,
        public ?ProsConsDto $prosCons,
        public ?InsuranceDto $insurance,
        public ?CurrencyIndexDto $currencyIndex,
        public ?CorrectionImpactDto $correctionImpact,
        public ?PromotionDto $promotion,
        public ?InterestDto $interest,
        public ?InstallmentDto $installment,
        public ?BankDto $bank,
        public ?AbilityDto $ability,
        public ?RequirementDto $requirement,
        public ?ExpertValueDto $expertValue,
        public ?InfoFieldsDto $infoFields,
        public ?DescriptionFieldsDto $descriptionFields
    ) {
    }
}
