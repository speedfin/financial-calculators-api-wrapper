<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class ProvisionDto
{
    public function __construct(
        public ?float $percent,
        public ?float $percentManual,
        public mixed $value,
        public ?bool $allowCrediting,
        public ?bool $credited,
        public ?float $creditedExceeded,
        public ?bool $forceCrediting,
        public ?bool $checkCrediting
    ) {
    }
}
