<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class InstallmentDto
{
    public function __construct(
        public ?EqualDto $equal,
        public ?DecreasingDto $decreasing,
        public ?string $periodChangesJson
    ) {
    }
}
