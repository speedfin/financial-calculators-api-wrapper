<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class InterestDto
{
    public function __construct(
        public mixed $value,
        public ?string $type,
        public ?FixedInterestRateDto $fixedInterestRate
    ) {
    }
}
