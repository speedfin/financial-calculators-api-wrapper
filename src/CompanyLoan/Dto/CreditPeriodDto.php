<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class CreditPeriodDto
{
    public function __construct(
        public ?int $value,
        public ?int $maxValue,
        public ?int $minValue,
        public ?int $gracePeriod,
    ) {
    }
}
