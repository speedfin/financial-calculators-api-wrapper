<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class CorrectionDto
{
    public function __construct(
        public ?int $id,
        public ?string $description,
        public ?string $name,
        public ?int $period,
        public ?float $costValue,
        public ?string $paymentValueType,
        public ?float $paymentValue,
        public null|array|int $installmentValue,
        public ?float $monthlyCostValue,
        public ?int $frequency,
        public ?string $paymentBaseText,
        public ?string $filterAlertLabel,
        public ?int $logicSequencePriority,
        public ?int $ageFrom,
        public ?int $ageTo,
        public ?string $displayName,
        public ?string $type,
        public ?bool $creditingPossibility,
        public ?bool $required,
        public ?bool $checkNotRequired,
        public ?bool $credited,
        public ?bool $selected,
        public ?bool $logicSequenceRequired,
        public ?bool $onlyInfo,
        public ?bool $enabledAgeType,
        public ?bool $forceCreditingFlag,
        public ?bool $creditingDisabled
    ) {
    }
}
