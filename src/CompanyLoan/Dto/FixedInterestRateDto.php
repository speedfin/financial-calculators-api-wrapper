<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class FixedInterestRateDto
{
    public function __construct(
        public mixed $periodFrom,
        public mixed $periodTo,
        public mixed $value,
        public ?float $valueManual
    ) {
    }
}
