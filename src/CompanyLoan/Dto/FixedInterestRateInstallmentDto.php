<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class FixedInterestRateInstallmentDto
{
    public function __construct(
        public ?float $installmentValue,
        public ?float $value
    ) {
    }
}
