<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class DescriptionFieldsDto
{
    public function __construct(
        public ?string $requiredMinimumAccountRecipients,
        public ?string $additionalIncome,
        public ?string $companyProductsConsolidation,
        public ?string $consolidationOfConsumerProducts,
        public ?string $methodOfEstimatingCapacity,
        public ?string $capacityCalculation,
        public ?string $consentOfSpouse
    ) {
    }
}
