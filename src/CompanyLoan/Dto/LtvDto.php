<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class LtvDto
{
    public function __construct(
        public ?float $value,
        public ?float $maxValue
    ) {
    }
}
