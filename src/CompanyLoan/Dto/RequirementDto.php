<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class RequirementDto
{
    public function __construct(
        public ?string $additionalRequirements,
        public ?bool $propertyCreditCardRequired,
        public ?bool $propertyVacationCredit,
        public ?bool $propertyProlongation,
        public ?bool $propertyAdditionalAmount,
        public ?bool $promotion
    ) {
    }
}
