<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CollectionFilterDto;

final readonly class ProductTypesDto
{
    public function __construct(
        public ?string $identifier,
        public ?string $label
    ) {
    }
}
