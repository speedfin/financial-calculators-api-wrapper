<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CollectionFilterDto;

final readonly class FiltersDto
{
    public function __construct(
        public ?array $economicActivity,
        public ?array $taxationForm,
        public ?array $productType,
        public ?array $security,
        public ?array $securityType,
        public ?array $various
    ) {
    }
}
