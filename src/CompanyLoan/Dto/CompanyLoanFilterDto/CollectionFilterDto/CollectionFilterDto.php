<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CollectionFilterDto;

final readonly class CollectionFilterDto
{
    public function __construct(
        /**
         * @var EconomicActivityDto[]|null
         */
        public ?array $economicActivities,
        /**
         * @var TaxationFormDto[]|null
         */
        public ?array $taxationForms,
        /**
         * @var ProductTypesDto[]|null
         */
        public ?array $productTypes,
        /**
         * @var SecuritiesDto[]|null
         */
        public ?array $securities,
        /**
         * @var SecurityTypesDto[]|null
         */
        public ?array $securityTypes,
        /**
         * @var VariousDto[]|null
         */
        public ?array $various
    ) {
    }

    public function getEconomicActivityIdentifierByLabel(string $label): ?string
    {
        /** @var EconomicActivityDto $item */
        foreach ($this->economicActivities as $item) {
            if ($item->label === $label) {
                return $item->identifier;
            }
        }

        return null;
    }

    public function getSecurityTypeIdentifierByLabel(string $label): ?string
    {
        foreach ($this->securityTypes as $item) {
            if ($item->label === $label) {
                return $item->identifier;
            }
        }

        return null;
    }

    public function getTaxationFormIdentifierByLabel(string $label): ?string
    {
        /** @var TaxationFormDto $item */
        foreach ($this->taxationForms as $item) {
            if ($item->label === $label) {
                return $item->identifier;
            }
        }

        return null;
    }

    public function getProductTypesIndentifiers(): array
    {
        $result = [];
        foreach ($this->productTypes as $productType) {
            $result[$productType->identifier] = $productType->identifier;
        }

        return $result;
    }
}
