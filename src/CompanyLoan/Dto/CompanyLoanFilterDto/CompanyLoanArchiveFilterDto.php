<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto;

final readonly class CompanyLoanArchiveFilterDto
{
    public function __construct(
        /**
         * @var CompanyLoanFilterDto[]
         */
        public ?array $filters
    ) {
    }
}
