<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\SliceAndSortableFilterDto;

final  readonly class SliceFilterDto
{
    public function __construct(
        public int $offset = 0,
        public int $length = 10
    ) {
    }
}
