<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class EqualFilterDto
{
    public function __construct(
        public ?FixedInterestRateFilterDto $fixedInterestRate,
    ) {
    }
}
