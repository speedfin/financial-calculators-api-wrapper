<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class CurrentFilterDto
{
    public function __construct(
        public ?string $name,
        public ?string $value
    ) {
    }
}
