<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class ItemFilterDto
{
    public function __construct(
        public ?BankFilterDto $bank,
        public ?MarginFilterDto $margin,
        public ?ProvisionFilterDto $provision,
        public ?ExpertValueFilterDto $expertValue,
        public ?CurrentFilterDto $current,
        /**
         * @var CorrectionFilterDto[]
         */
        public ?array $corrections,
        public ?int $id,
        public int|string|null $creditValueGross,
        public ?string $requestId,
        public ?CurrencyIndexFilterDto $currencyIndex,
        public ?InterestFilterDto $interest,
        public ?LtvFilterDto $ltv,
        public ?CurrencyFilterDto $currency,
        public ?InstallmentFilterDto $installment,
        public ?CostFilterDto $cost,
    ) {
    }
}
