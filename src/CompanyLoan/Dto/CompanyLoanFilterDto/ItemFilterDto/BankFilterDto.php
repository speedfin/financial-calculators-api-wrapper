<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class BankFilterDto
{
    public function __construct(
        public ?int $id
    ) {
    }
}
