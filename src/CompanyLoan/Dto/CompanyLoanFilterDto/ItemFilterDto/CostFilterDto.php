<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class CostFilterDto
{
    public function __construct(
        public ?float $costPerStandby,
        public ?float $costForRenewal,
        public ?int $periodForRenewal,
        public ?float $earlyRepaymentCost,
        public ?int $earlyRepaymentPeriod,
        public ?float $commissionForConsideration,
        public ?float $commissionForCommitment
    ) {
    }
}
