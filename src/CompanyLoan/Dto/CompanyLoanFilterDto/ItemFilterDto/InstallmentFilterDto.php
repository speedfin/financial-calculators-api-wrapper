<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class InstallmentFilterDto
{
    public function __construct(
        public ?EqualFilterDto $equal,
    ) {
    }
}
