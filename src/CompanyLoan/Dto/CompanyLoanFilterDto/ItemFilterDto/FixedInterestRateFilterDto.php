<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class FixedInterestRateFilterDto
{
    public function __construct(
        public ?float $valueManual,
        public ?float $value,
        public ?int $periodFrom,
        public ?int $periodTo
    ) {
    }
}
