<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class LtvFilterDto
{
    public function __construct(
        public ?float $value,
        public ?float $maxValue
    ) {
    }
}
