<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class ProvisionFilterDto
{
    public function __construct(
        public float|string|null $percent,
        public ?float $percentManual,
        public ?bool $credited
    ) {
    }
}
