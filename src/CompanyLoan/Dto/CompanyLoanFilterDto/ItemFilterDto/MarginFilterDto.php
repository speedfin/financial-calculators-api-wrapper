<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class MarginFilterDto
{
    public function __construct(
        public float|string|null $value,
        public ?float $valueManual
    ) {
    }
}
