<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class CurrencyFilterDto
{
    public function __construct(
        public ?int $id,
        public ?float $value,
        public ?string $iso,
        public ?string $symbol,
        public ?bool $showForeign
    ) {
    }
}
