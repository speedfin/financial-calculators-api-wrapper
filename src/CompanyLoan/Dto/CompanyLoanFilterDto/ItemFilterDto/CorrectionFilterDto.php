<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto;

final readonly class CorrectionFilterDto
{
    public function __construct(
        public ?int $id,
        public ?bool $checked,
        public ?bool $credited
    ) {
    }
}
