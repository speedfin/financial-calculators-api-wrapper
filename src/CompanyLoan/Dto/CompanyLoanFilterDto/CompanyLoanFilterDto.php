<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto;

use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\ItemFilterDto\ItemFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\SliceAndSortableFilterDto\SliceFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\SliceAndSortableFilterDto\SortableFilterDto;

final readonly class CompanyLoanFilterDto
{
    public function __construct(
        public ?SliceFilterDto $slice,
        public ?SortableFilterDto $sortable,
        public ?array $banks,
        public ?array $ids,
        public ?int $firmId,
        public ?bool $clientInternal,
        public ?bool $clientExternal,
        public ?int $incomeValue,
        public ?float $currencyIndex,
        public ?string $economicActivity,
        public ?string $taxationForm,
        public ?string $securityType,
        public ?string $companyStartUp,
        public ?array $productTypes,
        public ?array $securities,
        public ?array $various,
        public ?array $currencies,
        public ?bool $fixedInterestRate,
        public ?bool $variableInterestRate,
        public ?ItemFilterDto $item,
        public ?bool $indexWibor,
        public ?bool $indexWiron,
        public ?int $creditValue = 200000,
        public ?int $secureValue = 400000,
        public ?int $investmentValue = 400000,
        public ?int $creditPeriod = 48,
        public ?int $creditClientAge = 30
    ) {
    }

    public function getLtv(): float
    {
        return round(($this->creditValue / $this->secureValue * 100), 2);
    }

    public function getLtc(): float
    {
        return round(($this->creditValue / $this->investmentValue * 100), 2);
    }
}
