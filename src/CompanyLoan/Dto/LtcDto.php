<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class LtcDto
{
    public function __construct(
        public ?bool $enabled,
        public ?float $value
    ) {
    }
}
