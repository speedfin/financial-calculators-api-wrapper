<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class DecreasingDto
{
    public function __construct(
        public mixed $monthly,
        public mixed $monthlyTotal,
        public mixed $total,
        public ?bool $enabled,
        public ?FixedInterestRateInstallmentDto $fixedInterestRate,
        public ?float $interestValue,
        public ?PeriodChangeDto $periodChange
    ) {
    }
}
