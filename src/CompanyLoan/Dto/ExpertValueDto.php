<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Dto;

final readonly class ExpertValueDto
{
    public function __construct(
        public ?string $comment,
        public ?int $otherCost
    ) {
    }
}
