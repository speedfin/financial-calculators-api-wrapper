<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Adapter;

use Speedfin\Calculators\Common\Adapter\AbstractAdapter;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CompanyLoanArchiveFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CompanyLoanFilterDto;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanListDto;
use Speedfin\Calculators\Service\Deserialize;

class CompanyLoanAdapter extends AbstractAdapter
{
    public function getCollection(CompanyLoanFilterDto $filters): CompanyLoanListDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), CompanyLoanListDto::class);
    }

    public function getArchiveCollection(CompanyLoanArchiveFilterDto $filters): CompanyLoanListDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), CompanyLoanListDto::class);
    }
    
    public function getItem(CompanyLoanFilterDto $filters): CompanyLoanDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), CompanyLoanDto::class);
    }
}