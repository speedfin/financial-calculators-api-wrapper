<?php

declare(strict_types=1);

namespace Speedfin\Calculators\CompanyLoan\Adapter;

use Speedfin\Calculators\Common\Adapter\AbstractAdapter;

class CompanyLoanCollectionFilterAdapter extends AbstractAdapter
{
    public function getFilter(): array
    {
        return $this->sendGetRequest()->toArray();
    }
}