<?php

namespace Speedfin\Calculators\Bank\Service;

use Speedfin\Calculators\Bank\Dto\BankDto;
use Speedfin\Calculators\Bank\Dto\BankFilterDto;
use Speedfin\Calculators\Bank\Dto\BankListDto;
use Speedfin\Calculators\Common\Service\JWTInterface;

interface BankInterface extends JWTInterface
{
    const LIST_API_ENDPOINT = 'api/v3/bank';
    const ITEM_API_ENDPOINT = 'api/v2/bank/{id}';

    public function getList(?BankFilterDto $filter = null): BankListDto;

    public function getItem(int $id): BankDto;
}