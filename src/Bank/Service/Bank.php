<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Bank\Service;

use Speedfin\Calculators\Common\Service\AbstractProduct;
use Speedfin\Calculators\Bank\Adapter\BankAdapter;
use Speedfin\Calculators\Bank\Dto\BankFilterDto;
use Speedfin\Calculators\Bank\Dto\BankListDto;
use Speedfin\Calculators\Bank\Dto\BankDto;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class Bank extends AbstractProduct implements BankInterface
{
    public function getList(?BankFilterDto $filter = null): BankListDto
    {
        $bankAdapter = new BankAdapter($this->getUrl(self::LIST_API_ENDPOINT), $this->token);
        $cache = new FilesystemAdapter(
            '',
            14400,
            $this->cacheDir
        );

        return $cache->get(
            sprintf('bank-list-%s', md5(json_encode((array) $filter))),
            function (ItemInterface $item) use ($bankAdapter, $filter) {
                return $bankAdapter->getCollection($filter);
        });
    }

    public function getItem(int $id): BankDto
    {
        $bankAdapter = new BankAdapter(
            str_replace('{id}', (string)$id, $this->getUrl(self::ITEM_API_ENDPOINT)),
            $this->token
        );
        $cache = new FilesystemAdapter(
            '',
            14400,
            $this->cacheDir
        );

        return $cache->get(sprintf('bank-item-%s', (string)$id), function (ItemInterface $item) use ($bankAdapter) {
            return $bankAdapter->getItem();
        });
    }
}