<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Bank\Dto;

final readonly class BankListDto
{
    public function __construct(
        /**
         * @var BankDto[]|null
         */
       public ?array $items
    ) {
    }

    public function getItemBySlug(string $slug): ?BankDto
    {
        /** @var BankDto $item */
        foreach ($this->items as $item) {
            if ($item->slug === $slug) {
                return $item;
            }
        }

        return null;
    }
}
