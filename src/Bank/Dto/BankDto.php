<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Bank\Dto;

final readonly class BankDto
{
    public function __construct(
        public ?int $id,
        public ?string $slug,
        public ?string $name,
        public ?LogoDto $logo,
        public ?string $shortDescription,
        public ?string $description
    ) {
    }
}
