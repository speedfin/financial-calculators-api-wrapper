<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Bank\Dto;

final readonly class BankFilterDto
{
    public function __construct(
        public ?string $productType,
        public ?int $firmId
    ) {
    }
}
