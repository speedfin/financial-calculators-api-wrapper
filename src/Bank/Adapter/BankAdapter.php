<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Bank\Adapter;

use Speedfin\Calculators\Bank\Dto\BankDto;
use Speedfin\Calculators\Common\Adapter\AbstractAdapter;
use Speedfin\Calculators\Bank\Dto\BankFilterDto;
use Speedfin\Calculators\Bank\Dto\BankListDto;
use Speedfin\Calculators\Service\Deserialize;

class BankAdapter extends AbstractAdapter
{
    public function getCollection(BankFilterDto $filters): BankListDto
    {
        $data = $this->sendGetRequest((array) $filters);

        return Deserialize::deserialize($data->getContent(), BankListDto::class);
    }

    public function getItem(): BankDto
    {
        $data = $this->sendGetRequest();

        return Deserialize::deserialize($data->getContent(), BankDto::class);
    }
}