<?php

namespace Speedfin\Calculators\Common\Service;

use Speedfin\Calculators\Common\Dao\AbstractProductListDto;
use Speedfin\Calculators\Common\Dao\ProductPrintFilter;

interface ProductPrintable
{
    public function calculatePrint(ProductPrintFilter $productPrintFilter): AbstractProductListDto;
}