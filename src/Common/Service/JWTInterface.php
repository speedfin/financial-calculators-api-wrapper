<?php

namespace Speedfin\Calculators\Common\Service;

interface JWTInterface
{
    public function setToken(string $token): void;
}