<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Service;

use Behat\Gherkin\Loader\YamlFileLoader;
use Speedfin\Calculators\Common\Dao\AbstractProductFilter;
use Speedfin\Calculators\CompanyLoan\Dto\CompanyLoanFilterDto\CompanyLoanFilterDto;
use Speedfin\Calculators\MoneyLoan\Dto\MoneyLoanFilterDto\MoneyLoanFilterDto;
use Speedfin\Calculators\Mortgage\Dto\MortgageFilterDto\MortgageFilterDto;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\FileLoader;

abstract class AbstractProduct
{
    public string $url;
    public string $cacheDir;
    public string $token;
    public bool $cache = false;

    public function __construct(
        string $url,
        string $cacheDir
    )
    {
        $this->url = $url;
        $this->cacheDir = $cacheDir;
    }

    public function setCache(bool $cache): void
    {
        $this->cache = $cache;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }
    
    public function getCacheKey(AbstractProductFilter|MortgageFilterDto|CompanyLoanFilterDto|MoneyLoanFilterDto $filters, string $name): string
    {
        return sprintf('%s-%s',
            $name,
            md5(json_encode((array) $filters))
        );
    }
    
    protected function getUrl(string $path): string
    {
        return sprintf('%s/%s', $this->url, $path);
    }
}