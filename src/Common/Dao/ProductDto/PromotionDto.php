<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class PromotionDto extends DataTransferObject
{
    public ?string $dateFrom;
    public ?string $dateTo;
    public ?string $name;
}