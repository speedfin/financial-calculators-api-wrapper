<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class RequirementDto extends DataTransferObject
{
    public ?string $additionalRequirements;
}