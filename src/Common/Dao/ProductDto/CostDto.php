<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class CostDto extends DataTransferObject
{
    public ?float $initial;
    public ?float $total;
    public ?float $valuation;
    public ?float $creditValue;
    public ?float $creditValueGross;
}