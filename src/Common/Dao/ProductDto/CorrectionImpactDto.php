<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class CorrectionImpactDto extends DataTransferObject
{
    #[CastWith(BooleanCaster::class)]
    public ?bool $creditingPossibilityExceeded;
    public ?bool $forceCreditingPossibility;
    #[CastWith(FloatCaster::class)]
    public ?float $manualMarginFromCheckNotRequired;
    #[CastWith(FloatCaster::class)]
    public ?float $manualProvisionFromCheckNotRequired;
}