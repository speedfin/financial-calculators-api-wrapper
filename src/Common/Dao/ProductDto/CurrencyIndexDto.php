<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class CurrencyIndexDto extends DataTransferObject
{
    public ?int $id;
    public ?string $value;
}