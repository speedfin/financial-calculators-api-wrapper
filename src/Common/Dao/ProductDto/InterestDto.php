<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;

class InterestDto extends DataTransferObject
{
    public ?float $value;
    public ?string $type;
}