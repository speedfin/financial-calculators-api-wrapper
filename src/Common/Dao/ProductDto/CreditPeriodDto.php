<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class CreditPeriodDto extends DataTransferObject
{
    public ?int $value;
    public ?int $maxValue;
    public ?int $minValue;
}