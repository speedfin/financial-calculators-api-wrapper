<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class ProsConsDto extends DataTransferObject
{
    public ?array $pros;
    public ?array $cons;
    public ?string $prosCons;
}