<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class MarginDto extends DataTransferObject
{
    public ?float $value;
    public ?float $valueManual;
    public ?float $futureValue;
}