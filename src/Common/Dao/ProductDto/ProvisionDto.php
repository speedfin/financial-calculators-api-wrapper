<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;

class ProvisionDto extends DataTransferObject
{
    public ?float $percent;
    public ?float $percentManual;
    public ?float $value;
    #[CastWith(BooleanCaster::class)]
    public ?bool $credited;
    #[CastWith(BooleanCaster::class)]
    public ?bool $allowCrediting;
}