<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto\Installment;

use Spatie\DataTransferObject\DataTransferObject;

class EqualDto extends DataTransferObject
{
    public ?float $monthly;
    public ?float $monthlyTotal;
    public ?float $total;
    public ?float $interestValue;
}