<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto\Installment;

use Spatie\DataTransferObject\DataTransferObject;

class DecreasingDto extends DataTransferObject
{
    public ?float $monthly;
    public ?float $monthlyTotal;
    public ?float $total;
}