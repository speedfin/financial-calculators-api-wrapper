<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class RrsoDto extends DataTransferObject
{
    public ?float $value;
}