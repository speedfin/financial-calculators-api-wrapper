<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class InfoFieldsDto extends DataTransferObject
{
    public ?string $provision;
}