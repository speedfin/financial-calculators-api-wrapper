<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Bank\Dao\BankDto\LogoDto;

class BankDto extends DataTransferObject
{
    public ?int $id;
    public ?string $slug;
    public ?string $name;
    public ?int $processingTime;
    public ?LogoDto $logo;
}