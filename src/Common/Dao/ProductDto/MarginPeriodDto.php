<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductDto;

use Spatie\DataTransferObject\DataTransferObject;

class MarginPeriodDto extends DataTransferObject
{
    public ?int $periodFrom;
    public ?int $periodTo;
    public ?float $interestRate;
    public ?float $surchargeRate;
}