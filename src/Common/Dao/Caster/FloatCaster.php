<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\Caster;

use Spatie\DataTransferObject\Caster;

class FloatCaster implements Caster
{
    public function __construct(
        private array $type
    ) {
    }

    public function cast(mixed $value): ?float
    {
        if ($value === null) {
            return null;
        }
        if ((float) $value === $value) {
            return $value;
        }
        $sanitized = preg_replace("/[^0-9,.-]/", "", (string) $value);
        $float = floatval(str_replace(',', '.', $sanitized));
        return round($float, 2);
    }
}