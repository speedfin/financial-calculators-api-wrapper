<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\Caster;

use Spatie\DataTransferObject\Caster;

class IntCaster implements Caster
{
    public function __construct(
        private array $type,
        private ?int $default
    ) {
    }

    public function cast(mixed $value): ?int
    {
        if ($value === "" || $value === null) {
            return $this->default;
        }

        return (int) preg_replace("/[^0-9-]/", "", (string)$value);
    }
}