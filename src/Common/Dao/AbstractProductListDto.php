<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao;

use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Mortgage\Dao\MortgageDto;

abstract class AbstractProductListDto extends DataTransferObject
{
    const DIRECTION_ASC = 'ASC';
    const DIRECTION_DESC = 'DESC';

    public ?array $products = [];

    public function getProducts(): array
    {
        return $this->products;
    }

    public function first(): ?AbstractProductDto
    {
        $first = reset($this->products);
        return $first ?: null;
    }

    public function last(): ?AbstractProductDto
    {
        $last = end($this->products);
        return $last ?: null;
    }

    public function count(): int
    {
        return count($this->products);
    }

    public function slice(int $offset, int $length): self
    {
        $this->products = array_slice($this->products, $offset, $length);

        return $this;
    }

    /**
     * Sorts by the given properties depth. Eg. sorting by $mortgageDto->installment->equal->monthly will be
     * $mortgageListDto->sort('ASC', 'installment', 'equal', 'monthly')
     */
    public function sort(string $direction, string $field, string $subField = null, string $subSubField = null): self
    {
        usort($this->products, function ($item1, $item2) use ($field, $subField, $subSubField) {
            if ($field && $subField && $subSubField) {
                return $item1->$field->$subField->$subSubField <=> $item2->$field->$subField->$subSubField;
            } elseif ($field && $subField) {
                return $item1->$field->$subField <=> $item2->$field->$subField;
            } else {
                return $item1->$field <=> $item2->$field;
            }
        });

        if ($direction === self::DIRECTION_DESC) {
            $this->products = array_reverse($this->products);
        }

        return $this;
    }

    /**
     * $mode = true, leaves only those objects in $this->products array, where MortgageDto property $field value is in $values array.
     * $mode = false, removes those objects.
     */
    public function filter(string $field, array $values, $mode = true): self
    {
        $filtered = array_filter($this->products, function($product) use ($field, $values, $mode) {
            if (in_array($product->$field, $values)) {
                return $mode;
            }

            return !$mode;
        });

        //Rebase array keys after unsetting elements
        $this->products = array_values($filtered);

        return $this;
    }
}