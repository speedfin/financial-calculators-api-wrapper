<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao;

use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\ProductDto\BankDto;
use Speedfin\Calculators\Common\Dao\ProductDto\CostDto;
use Speedfin\Calculators\Common\Dao\ProductDto\CreditPeriodDto;
use Speedfin\Calculators\Common\Dao\ProductDto\ExpertValueDto;
use Speedfin\Calculators\Common\Dao\ProductDto\MarginDto;
use Speedfin\Calculators\Common\Dao\ProductDto\PromotionDto;
use Speedfin\Calculators\Common\Dao\ProductDto\ProsConsDto;
use Speedfin\Calculators\Common\Dao\ProductDto\ProvisionDto;
use Speedfin\Calculators\Common\Dao\ProductDto\CurrencyIndexDto;

abstract class AbstractProductDto extends DataTransferObject
{
    const MORTGAGE_TYPE = 'mortgage';
    const MORTGAGE_LOAN_TYPE = 'mortgage_loan';
    const MONEY_LOAN_TYPE = 'money_loan';
    const COMPANY_LOAN_TYPE = 'company_loan';

    public ?ProsConsDto $prosCons;
    public ?BankDto $bank;
    public ?ExpertValueDto $expertValue;
    
    public ?int $id;
    public ?int $cloneId;
    public ?string $requestId;
    public ?string $title;
    public ?string $description;
}