<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;
use Speedfin\Calculators\Common\Dao\Caster\IntCaster;
use Speedfin\Calculators\Common\Dao\ProductFilter\SliceFilter;
use Speedfin\Calculators\Common\Dao\ProductFilter\SortableFilter;

abstract class AbstractProductFilter extends DataTransferObject
{
    public ?SliceFilter $slice;
    public ?SortableFilter $sortable;
    #[CastWith(IntCaster::class, default: 30)]
    public ?int $creditClientAge = 30;
    #[CastWith(IntCaster::class, default: 1)]
    public ?array $banks;
    public ?array $ids;
    //Set firmId if you want to cache response based on filters
    public ?int $firmId;
    #[CastWith(BooleanCaster::class)]
    public ?bool $clientInternal;
    #[CastWith(BooleanCaster::class)]
    public ?bool $clientExternal;

    public function setBanks(array $banks): self
    {
        $this->banks = $banks;

        return $this;
    }
    
    public function addSortable(): SortableFilter
    {
        return $this->sortable = new SortableFilter();
    }

    public function addSlice(): SliceFilter
    {
        return $this->slice = new SliceFilter();
    }

    public function setCreditClientAge(int $creditClientAge): self
    {
        $this->creditClientAge = $creditClientAge;

        return $this;
    }

    public function setIds(array $ids): self
    {
        $this->ids = $ids;

        return $this;
    }
}