<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;
use Speedfin\Calculators\Common\Dao\Caster\IntCaster;
use Speedfin\Calculators\Common\Dao\ProductFilter\Item\BankFilter;
use Speedfin\Calculators\Common\Dao\ProductFilter\Item\CorrectionFilter;
use Speedfin\Calculators\Common\Dao\ProductFilter\Item\CurrencyIndexFilter;
use Speedfin\Calculators\Common\Dao\ProductFilter\Item\CurrentFilter;
use Speedfin\Calculators\Common\Dao\ProductFilter\Item\ExpertValueFilter;
use Speedfin\Calculators\Common\Dao\ProductFilter\Item\InterestFilter;
use Speedfin\Calculators\Common\Dao\ProductFilter\Item\MarginFilter;
use Speedfin\Calculators\Common\Dao\ProductFilter\Item\ProvisionFilter;

/**
 * Single product filters
 */
class ItemFilter extends DataTransferObject
{
    public ?BankFilter $bank;
    public ?MarginFilter $margin;
    public ?ProvisionFilter $provision;
    public ?ExpertValueFilter $expertValue;
    public ?CurrentFilter $current;
    #[CastWith(ArrayCaster::class, itemType: CorrectionFilter::class)]
    public ?array $corrections;

    public ?int $id;
    #[CastWith(IntCaster::class, default: 0)]
    public ?int $creditValueGross;
    public ?string $requestId;
}