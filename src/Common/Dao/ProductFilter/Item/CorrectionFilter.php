<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter\Item;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;

class CorrectionFilter extends DataTransferObject
{
    public ?int $id;
    #[CastWith(BooleanCaster::class)]
    public ?bool $checked;
    #[CastWith(BooleanCaster::class)]
    public ?bool $credited;
}