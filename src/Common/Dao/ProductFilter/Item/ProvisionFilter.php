<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter\Item;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class ProvisionFilter extends DataTransferObject
{
    #[CastWith(FloatCaster::class)]
    public ?float $percent;
    #[CastWith(FloatCaster::class)]
    public ?float $percentManual;
    #[CastWith(BooleanCaster::class)]
    public ?bool $credited;
}