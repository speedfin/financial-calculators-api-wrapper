<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter\Item;

use Spatie\DataTransferObject\DataTransferObject;

class ExpertValueFilter extends DataTransferObject
{
    public ?int $otherCost;
    public ?string $comment;
}