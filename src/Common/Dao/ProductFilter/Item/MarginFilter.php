<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter\Item;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;

class MarginFilter extends DataTransferObject
{
    #[CastWith(FloatCaster::class)]
    public ?float $value;
    #[CastWith(FloatCaster::class)]
    public ?float $valueManual;
}