<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter\Item;

use Spatie\DataTransferObject\DataTransferObject;

class BankFilter extends DataTransferObject
{
    public ?int $id;
}