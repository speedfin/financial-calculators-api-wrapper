<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter\Item;

use Spatie\DataTransferObject\DataTransferObject;

class InstallmentFilter extends DataTransferObject
{
    public ?string $periodChangesJson;
}