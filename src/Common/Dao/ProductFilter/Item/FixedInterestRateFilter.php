<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter\Item;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\FloatCaster;
use Speedfin\Calculators\Common\Dao\Caster\IntCaster;

class FixedInterestRateFilter extends DataTransferObject
{
    #[CastWith(FloatCaster::class)]
    public ?float $valueManual;
    #[CastWith(FloatCaster::class)]
    public ?float $value;
    #[CastWith(IntCaster::class, default: null)]
    public ?int $periodFrom;
    #[CastWith(IntCaster::class, default: null)]
    public ?int $periodTo;
}