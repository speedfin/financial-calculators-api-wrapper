<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter;

use Spatie\DataTransferObject\DataTransferObject;

class SliceFilter extends DataTransferObject
{
    public int $offset = 0;
    public int $length = 10;

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    public function setLength(int $length): self
    {
        $this->length = $length;

        return $this;
    }
}