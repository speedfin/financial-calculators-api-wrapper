<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Dao\ProductFilter;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;
use Speedfin\Calculators\Common\Dao\Caster\BooleanCaster;

class SortableFilter extends DataTransferObject
{
    const DIRECTION_ASC = 'ASC';
    const DIRECTION_DESC = 'DESC';

    public string $field = 'id';
    public string $direction = self::DIRECTION_ASC;
    public ?string $subField;
    #[CastWith(BooleanCaster::class)]
    public ?bool $bankShuffle;
    #[CastWith(BooleanCaster::class)]
    public ?bool $bankUnique;

    public function setField(string $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function setDirection(string $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function setSubField(?string $subField): self
    {
        $this->subField = $subField;

        return $this;
    }

    public function shuffleByBank(): self
    {
        $this->bankShuffle = true;

        return $this;
    }

    public function uniqueBank(): self
    {
        $this->bankUnique = true;

        return $this;
    }
}