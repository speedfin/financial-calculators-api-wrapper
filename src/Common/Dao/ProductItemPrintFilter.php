<?php

namespace Speedfin\Calculators\Common\Dao;

class ProductItemPrintFilter
{
    public int $id;
    public ?string $request_id = null;
    public int $sort;
    public array $correctionsToPrint = [];
}