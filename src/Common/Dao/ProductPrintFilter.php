<?php

namespace Speedfin\Calculators\Common\Dao;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Casters\ArrayCaster;

abstract class ProductPrintFilter
{
    #[CastWith(ArrayCaster::class, itemType: ProductItemPrintFilter::class)]
    public ?array $items;
    
    public ?array $fields = [];

    #[CastWith(BooleanCaster::class, default: false)]
    public ?string $namevis = '';
}