<?php

namespace Speedfin\Calculators\Common\Enum;

enum InterestType: string
{
    case VARIABLE_INTEREST_RATE_TYPE = 'variable_interest_rate';
    case FIXED_INTEREST_RATE_TYPE = 'fixed_interest_rate';
    case PERIODICAL_INTEREST_RATE_TYPE = 'periodical_interest_rate';
}
