<?php

declare(strict_types=1);

namespace Speedfin\Calculators\Common\Adapter;

use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response\CurlResponse;

abstract class AbstractAdapter
{
    public string $url;
    public string $token;
    public CurlHttpClient $client;

    public function __construct(string $url, string $token)
    {
        $this->client = HttpClient::create();
        $this->url = $url;
        $this->token = $token;
    }

    protected function sendGetRequest(?array $filters = []): CurlResponse
    {
        return $this->client->request(
            'GET',
            $this->url,
            [
                'query' => $filters,
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }

    protected function sendPostRequest(?array $filters = []): CurlResponse
    {
        return $this->client->request(
            'POST',
            $this->url,
            [
                'body' => $filters,
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Accept' => 'application/json'
                ]
            ]
        );
    }
}