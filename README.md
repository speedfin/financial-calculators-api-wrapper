# FinancialCalculatorsBundle

Wrapper for speedfin financial calculators API.

## Installation

The bundle can be installed using Composer 

```
composer require speedfin/financial-calculators
```

Enable bundle in config/bundles.php 

```
Speedfin\Calculators\FinancialCalculatorsBundle::class => ['all' => true],
```

Create config file speedfin_financial_calculators.yaml in config/packages/

```yaml
speedfin_financial_calculators:
    jwt_token: aaa.bbb.ccc
    api_domain: 'http://calculator.localhost:8083'
```

### Parameters:

#### `jwt_token`

_Required_

JWT generate from https://api.speedfin.pl/api/login_check

## Usage

You can set values to a filter object either by passing an array to the constructor or by using setters:

```php
use Speedfin\Calculators\Mortgage\Service\Mortgage;
use Speedfin\Calculators\Mortgage\Dao\MortgageFilter;

$mortgageFilter = new MortgageFilter([
    'secureValue' => 300000,
    'banks' => [1,3,4],
    'ability' => [
        'households' => [
            [
                'cars' => 1,
                'status' => 'das',
                'region' => 'das',
                'applicants' => [
                    [
                        'name' => 'Imię',
                        'incomes' => [
                            [
                                'id' => 3,
                                'value' => 3,
                            ]
                        ],
                        'outcomes' => [
                            [
                                'id' => '4'
                            ]
                        ]
                    ]
                ]
            ],
            [
                'cars' => 2,
                'status' => 'das',
                'region' => 'das',
                'applicants' => [
                    [
                        'name' => 'Imię',
                        'incomes' => [
                            [
                                'id' => 3,
                                'value' => 3,
                            ]
                        ],
                        'outcomes' => [
                            [
                                'id' => '4'
                            ]
                        ]
                    ]
                ]
            ]
        ]
]
]);

$mortgageFilter
    ->setCreditPurpose(2)
    ->setCreditValue(250000);
    
$mortgageFilter->addSlice()->setLength(23)->setOffset(5);

$ability = $mortgageFilter->addAbility();

$household = $ability
    ->addHousehold();

$applicant = $household
    ->addApplicant()
    ->setName('Rodzina x');

$applicant
    ->addOutcome()
    ->setId(12)
    ->setValue(323);

$applicant
    ->addIncome()
    ->setId(2)
    ->setPeriodForward(32)
    ->setValue(321)
    ->setPeriodForward(2);

$mortgageService = new Mortgage();
$mortgageService->calculate($mortgageFilter);

```